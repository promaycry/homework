package com.epam.homework_13.controller;

import com.epam.homework_13.entity.Country;
import com.epam.homework_13.service.CountryService;
import lombok.extern.log4j.Log4j2;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/country")
@Log4j2
public class CountryController {

    @Autowired
    private CountryService countryService;

    @PutMapping("/{name}")
    public Country create(@PathVariable(name = "name")String name){
      return  countryService.create(new Country(name));
    }

    @GetMapping("/{name}")
    public Country get(@PathVariable(name = "name")String name){
        printCacheInfo();
        return  countryService.get(name);
    }

    private static void printCacheInfo() {
        List<CacheManager> cacheManagers = CacheManager.ALL_CACHE_MANAGERS;
        if (!cacheManagers.isEmpty()) {
            CacheManager cacheManager = cacheManagers.get(0);
            Cache authorsCache = cacheManager.getCache(Country.class.getName());
           log.info("Second level cache has size = " + authorsCache.getSize());
        } else {
            log.info("Hibernate second level cache is disabled.");
        }
    }


}
