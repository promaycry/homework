package com.epam.homework_13.entity;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Date;
import java.util.UUID;

@Entity
@Data
@DynamicInsert
@DynamicUpdate
@NamedQueries({@NamedQuery(name = "developerByName",
        query = "from Developer d where d.name=:name")})
public class Developer {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(columnDefinition = "BINARY(16)",nullable = false,updatable = false)
    private UUID id;

    private String name;

    @ManyToOne
    private Company company;

    @Column(nullable = false,updatable = false)
    private Date birthDate;

}
