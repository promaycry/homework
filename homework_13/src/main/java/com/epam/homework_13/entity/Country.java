package com.epam.homework_13.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Cache;

import javax.persistence.Cacheable;


import javax.persistence.*;

import java.util.UUID;

@Entity
@DynamicInsert
@DynamicUpdate
@Data
@NoArgsConstructor
@NamedQueries({@NamedQuery(name = "countryByName",
        query = "from Country c where c.name=:name",hints = {@QueryHint(name = "org.hibernate.cacheable",value = "true")})})
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Country {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(columnDefinition = "BINARY(16)",nullable = false,updatable = false)
    private UUID id;

    private String name;

    public Country(String newName){
        this.name=newName;
    }

}
