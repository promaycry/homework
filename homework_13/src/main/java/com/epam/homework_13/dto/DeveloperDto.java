package com.epam.homework_13.dto;

import lombok.Data;

import java.sql.Date;
import java.util.UUID;

@Data
public class DeveloperDto {

    private Date birthDate;

    private UUID id;

    private String name;

}
