package com.epam.homework_13.dao;

import com.epam.homework_13.entity.Country;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Repository
public class CountryDaoImpl implements CountryDao{

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional
    public void create(Country country) {
        Session session=sessionFactory.getCurrentSession();
        session.save(country);
    }

    @Override
    @Transactional
    public Country get(UUID id) {
        Session session=sessionFactory.getCurrentSession();
        Country country=session.get(Country.class,id);
        return Objects.requireNonNull(country,"Country doesn`t exist");
    }

    @Override
    @Transactional
    public Country get(String name) {
        Session session=sessionFactory.getCurrentSession();
        Object result=session.getNamedQuery("countryByName")
                .setParameter("name",name).setCacheable(true)
                .getSingleResult();
        return (Country) result;
    }

    @Override
    @Transactional
    public List<Country>get(){
        Session session=sessionFactory.getCurrentSession();
        List<Country>countries=session.createQuery("select c from Country c").list();
        return countries;
    }

}
