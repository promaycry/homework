package com.epam.homework_13.dao;

import com.epam.homework_13.entity.Country;

import java.util.List;
import java.util.UUID;

public interface CountryDao {

    void create(Country country);

    Country get(UUID id);

    Country get(String name);

    List<Country> get();
}
