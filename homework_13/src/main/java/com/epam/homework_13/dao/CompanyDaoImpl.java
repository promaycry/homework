package com.epam.homework_13.dao;

import com.epam.homework_13.entity.Company;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.UUID;

@Repository
public class CompanyDaoImpl implements CompanyDao{

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional
    public void save(Company company) {
        Session session=sessionFactory.getCurrentSession();
        session.save(company);
    }

    @Override
    @Transactional
    public Company get(String name) {
        Session session=sessionFactory.getCurrentSession();
        Object result=session.getNamedQuery("companyByName").setParameter("name",name).getSingleResult();
        return (Company) result;
    }

    @Override
    @Transactional
    public Company get(UUID id) {
        Session session=sessionFactory.getCurrentSession();
        Company company=session.get(Company.class,id);
        return Objects.requireNonNull(company,"Company doesn`t exist");
    }

    @Override
    @Transactional
    public void delete(UUID id) {
        Company company=get(id);
        sessionFactory.getCurrentSession().delete(company);
    }



}
