package com.epam.homework_13.dao;

import com.epam.homework_13.entity.Company;

import java.util.UUID;

public interface CompanyDao {

    void save(Company company);

    Company get(String name);

    Company get(UUID id);

    void delete(UUID id);

}
