package com.epam.homework_13.service;

import com.epam.homework_13.dto.CompanyDto;
import com.epam.homework_13.dto.ApplicationDto;
import com.epam.homework_13.dto.DeveloperDto;
import com.epam.homework_13.entity.Company;
import com.epam.homework_13.entity.Application;
import com.epam.homework_13.entity.Developer;
import org.springframework.stereotype.Service;

@Service
public class CompanyAssemblerImpl implements CompanyAssembler{

    @Override
    public Company assemble(CompanyDto companyDto) {
        Company company=new Company();
        company.setName(companyDto.getName());
        company.setId(companyDto.getId());
        companyDto.getProducts().forEach(applicationDto -> company.getApplications()
                .add(assemble(applicationDto,company)));
        companyDto.getDevelopers().forEach(developerDto -> company.getDevelopers()
                .add(assemble(developerDto,company)));
        company.setCountry(companyDto.getCountry());
        return company;
    }

    @Override
    public CompanyDto assemble(Company company) {
        CompanyDto companyDto=new CompanyDto();
        companyDto.setName(company.getName());
        companyDto.setId(company.getId());
        company.getApplications()
                .forEach(application -> companyDto.getProducts()
                        .add(assemble(application)));
        company.getDevelopers().forEach(developer -> companyDto.getDevelopers()
                .add(assemble(developer)));
        companyDto.setCountry(company.getCountry());
        return companyDto;
    }

    public ApplicationDto assemble(Application application){
        ApplicationDto applicationDto =new ApplicationDto();
        applicationDto.setId(application.getId());
        applicationDto.setName(application.getName());
        return applicationDto;
    }

    public Application assemble(ApplicationDto applicationDto, Company company){
        Application application =new Application();
        application.setId(applicationDto.getId());
        application.setName(applicationDto.getName());
        application.setCompany(company);
        return application;
    }

    public DeveloperDto assemble(Developer developer){
        DeveloperDto developerDto=new DeveloperDto();
        developerDto.setBirthDate(developer.getBirthDate());
        developerDto.setName(developer.getName());
        developerDto.setId(developer.getId());
        return developerDto;
    }

    public Developer assemble(DeveloperDto developerDto, Company company){
        Developer developer =new Developer();
        developer.setId(developerDto.getId());
        developer.setName(developerDto.getName());
        developer.setBirthDate(developerDto.getBirthDate());
        developer.setCompany(company);
        return developer;
    }

}
