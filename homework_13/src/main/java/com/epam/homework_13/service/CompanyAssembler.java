package com.epam.homework_13.service;

import com.epam.homework_13.dto.CompanyDto;
import com.epam.homework_13.entity.Company;

public interface CompanyAssembler {

    Company assemble(CompanyDto companyDto);

    CompanyDto assemble(Company company);

}
