package com.epam.homework_13.service;

import com.epam.homework_13.dao.CountryDao;
import com.epam.homework_13.entity.Country;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class CountryServiceImpl implements CountryService {

    @Autowired
    CountryDao countryDao;

    @Override
    public Country create(Country country) {
        countryDao.create(country);
        return countryDao.get(country.getName());
    }

    @Override
    public Country get(UUID id) {
        return countryDao.get(id);
    }

    @Override
    public Country get(String name) {
        return countryDao.get(name);
    }

}
