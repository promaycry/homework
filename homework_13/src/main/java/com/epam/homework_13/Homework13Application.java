package com.epam.homework_13;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;

@SpringBootApplication(exclude= HibernateJpaAutoConfiguration.class)
public class Homework13Application {

    public static void main(String[] args) {
        SpringApplication.run(Homework13Application.class, args);
    }

}
