package com.epam.homework_15.errorhandler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.ErrorHandler;

@Component
@Slf4j
public class DefaultErrorHandler implements ErrorHandler {

    @Autowired
    public DefaultErrorHandler(JmsTemplate jmsTemplate){
        this.jmsTemplate=jmsTemplate;
    }

    private JmsTemplate jmsTemplate;

    @Override
    public void handleError(Throwable t) {
        jmsTemplate.convertAndSend("invalid messages",t.getMessage());
    }

}
