package com.epam.homework_15;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.jms.annotation.EnableJms;

@SpringBootApplication
@EnableJms
public class Homework15Application {

    public static void main(String[] args) {
        SpringApplication.run(Homework15Application.class, args);
    }

}
