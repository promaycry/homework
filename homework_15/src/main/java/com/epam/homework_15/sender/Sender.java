package com.epam.homework_15.sender;

import com.epam.homework_15.domain.Answer;
import com.epam.homework_15.domain.Message;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Component
@Slf4j
public class Sender {

    @Autowired
    public Sender(JmsTemplate jmsTemplate){
        this.jmsTemplate=jmsTemplate;
    }

    private static final Map<UUID, Message> requests= Collections.synchronizedMap(new HashMap<UUID,Message>());

    private JmsTemplate jmsTemplate;

    public void sendMessage(Message message){
        requests.put(message.getCorrelationId(),message);
        jmsTemplate.convertAndSend("message.queue", message);
    }

    @SneakyThrows
    @JmsListener(destination = "result.queue",concurrency = "10")
    public void receiveAnswer(@Payload Answer answer) {
        Message request=requests.get(answer.getCorrelationId());
        if(request==null){
            throw new IllegalArgumentException(String.format("Request with id %s is not exist ",answer.getCorrelationId()));
        }
        BigDecimal result=request.getFirstNumber().add(request.getSecondNumber());
        if(!result.equals(answer.getResult())){
            throw new IllegalArgumentException(String.format("Answer is incorrect for id %s must be %s but %s",
                    answer.getCorrelationId(), result,answer.getResult()));
        }
        requests.remove(answer.getCorrelationId());
        log.info("Result for request with id "+request.getCorrelationId()+ " is "+answer.getResult());
    }

}
