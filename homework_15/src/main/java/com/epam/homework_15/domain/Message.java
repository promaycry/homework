package com.epam.homework_15.domain;

import lombok.Data;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.UUID;

@Data
public class Message implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID correlationId;

    private BigDecimal firstNumber;

    private BigDecimal secondNumber;

}
