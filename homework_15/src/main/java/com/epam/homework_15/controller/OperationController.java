package com.epam.homework_15.controller;

import com.epam.homework_15.domain.Message;
import com.epam.homework_15.sender.Sender;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@Slf4j
public class OperationController {

    @Autowired
    public OperationController(Sender sender){
        this.sender=sender;
    }

    private Sender sender;

    @PostMapping("/send")
    public void send(){
        createMessages().forEach(message -> sender.sendMessage(message));
    }

    public static BigDecimal generateRandomBigDecimalFromRange(BigDecimal min, BigDecimal max) {
        BigDecimal randomBigDecimal = min.add(new BigDecimal(Math.random()).multiply(max.subtract(min)));
        return randomBigDecimal.setScale(2,BigDecimal.ROUND_HALF_UP);
    }

    public List<Message>createMessages(){
        List<Message>messages=new ArrayList<>();
        for(long i=1;i<22;i++){
            Message message=new Message();
            message.setSecondNumber(generateRandomBigDecimalFromRange(new BigDecimal("1"),new BigDecimal("100")));
            message.setCorrelationId(UUID.randomUUID());
            message.setFirstNumber(generateRandomBigDecimalFromRange(new BigDecimal("1"),new BigDecimal("100")));
            messages.add(message);
        }
        Message message=new Message();
        message.setFirstNumber(new BigDecimal("20.20"));
        message.setSecondNumber(new BigDecimal("20.21"));
        message.setCorrelationId(UUID.fromString("0f14d0ab-9605-4a62-a9e4-5ed26688389b"));
        messages.add(message);
        return messages;
    }

}
