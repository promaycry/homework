package com.epam.homework_15.receiver;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ErrorReceiver {

    @SneakyThrows
    @JmsListener(destination = "invalid messages")
    public void log(@Payload String cause){
        log.error(cause);
    }

}
