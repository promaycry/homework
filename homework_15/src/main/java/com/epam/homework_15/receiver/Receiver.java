package com.epam.homework_15.receiver;

import com.epam.homework_15.domain.Answer;
import com.epam.homework_15.domain.Message;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import java.math.BigDecimal;
import java.util.*;

@Component
@Slf4j
public class Receiver {

    @Autowired
    public Receiver(JmsTemplate jmsTemplate){
        this.jmsTemplate=jmsTemplate;
    }

    private static final  Map<UUID, Message> requests= Collections.synchronizedMap(new HashMap<UUID,Message>());

    private JmsTemplate jmsTemplate;

    private static final Message fakeMessage=new Message();

    static {
        fakeMessage.setFirstNumber(new BigDecimal(1));
        fakeMessage.setSecondNumber(new BigDecimal(2));
        fakeMessage.setCorrelationId(UUID.fromString("2f34d0ab-9605-4a62-a9e4-5ed26688141b"));
    }

    @SneakyThrows
    @JmsListener(destination = "message.queue")
    public void receive(@Payload Message message) {
            log.info("Receive message "+message.getCorrelationId());
            requests.put(message.getCorrelationId(),message);
            log.info("Requests size"+requests.size());
                if(requests.size()==22){
                    requests.get(UUID.fromString("0f14d0ab-9605-4a62-a9e4-5ed26688389b"))
                            .setSecondNumber(new BigDecimal(1));
                    requests.get(UUID.fromString("0f14d0ab-9605-4a62-a9e4-5ed26688389b"))
                            .setFirstNumber(new BigDecimal(1));
                    requests.put(fakeMessage.getCorrelationId(),fakeMessage);
                    Set<Map.Entry<UUID,Message>> set=requests.entrySet();
                    set.parallelStream().forEach(x->sendResponse((x.getValue())));
                    requests.clear();
                }
    }

    public void sendResponse(Message message){
        Answer answer=new Answer();
        answer.setCorrelationId(message.getCorrelationId());
        BigDecimal result=message.getFirstNumber().add(message.getSecondNumber());
        answer.setResult(result);
        jmsTemplate.convertAndSend("result.queue",answer);
    }

}
