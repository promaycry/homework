package com.epam.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * User entity.
 *
 * @author Kaliuha Aleksei
 *
 */
@Entity
@Table(name = "users")
public class User {

    @javax.persistence.Id
    private Long Id;

    public Long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    private static final long serialVersionUID = -6889036256149495388L;

    @Column(name = "login")
    private String login;

    public void setLogin(String login) {
        this.login = login;
    }

    public String getLogin() {
        return login;
    }

    @Column(name = "email")
    private String email;

    @Column(name = "money")
    private BigDecimal money;

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "User [login= "+login+" , id= "+getId()+" ]";
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof User)){
            return false;
        }
        return getId().equals(((User)obj).getId());
    }
}
