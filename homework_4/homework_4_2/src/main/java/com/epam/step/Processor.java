package com.epam.step;

import com.epam.entity.User;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.mail.SimpleMailMessage;

public class Processor implements ItemProcessor<User, SimpleMailMessage> {

	@Override
	public SimpleMailMessage process(User user) throws Exception {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setTo(user.getEmail());
		message.setSubject("money is low");
		message.setText("update your account");
		return message;
	}
}
