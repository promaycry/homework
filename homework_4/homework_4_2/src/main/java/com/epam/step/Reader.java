package com.epam.step;


import com.epam.entity.User;
import com.epam.repository.UserRepository;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;

import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Iterator;

public class Reader implements ItemReader<User> {

	@Autowired
	UserRepository userRepository;

	private Iterator<User>userIterator;

	@BeforeStep
	public void before(StepExecution execution){
		userIterator=userRepository.findAll().iterator();
	}

	@Override
	public User read() {
		if (userIterator != null && userIterator.hasNext()) {
			return userIterator.next();
		} else {
			return new User();
		}
	}

}