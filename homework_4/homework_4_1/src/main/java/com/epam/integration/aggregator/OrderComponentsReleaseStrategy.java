package com.epam.integration.aggregator;

import lombok.extern.slf4j.Slf4j;
import org.springframework.integration.aggregator.ReleaseStrategy;
import org.springframework.integration.store.MessageGroup;
import org.springframework.messaging.Message;

@Slf4j
public class OrderComponentsReleaseStrategy implements ReleaseStrategy {

    @Override
    public boolean canRelease(MessageGroup messageGroup) {
        for (Message<?> msg : messageGroup.getMessages()) {
          long  count=(long)msg.getHeaders().get("countOfNoCanceled");
          long now=messageGroup.getMessages().size();
          log.info("count-->"+count);
            if (now!=count) {
                return false;
            }
            else{
                log.info("release");
                return true;
            }
        }
        return true;
    }

}
