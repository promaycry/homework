package com.epam.integration.splitter;

import com.epam.model.Order;
import com.epam.model.OrderState;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.integration.splitter.AbstractMessageSplitter;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import java.util.*;

@Component("orderComponentsSplitter")
@Slf4j
public class OrderComponentsSplitter extends AbstractMessageSplitter {

    private Map<String, List<Message<?>>> splittedMessages = new HashMap<String, List<Message<?>>>();

    @Override
    protected Object splitMessage(Message<?> message) {
        Collection<Message<?>> messages = new ArrayList<Message<?>>();
        List<Order>orders = (List<Order>) message.getPayload();
        Iterator<?> iterator = orders.iterator();
        long count=orders.stream().filter(o->!(o.getOrderState().equals(OrderState.CANCELED))).count();
        log.info("count--->"+count);
        while (iterator.hasNext()) {
            Order order = (Order) iterator.next();
            Message<?> msg = MessageBuilder.withPayload(order).setHeaderIfAbsent("countOfNoCanceled",count)
                    .build();
            messages.add(msg);
            addMessage(""+order.getId(), msg);
        }
        return messages;
    }

    public Map<String, List<Message<?>>> getSplittedMessages() {
        return this.splittedMessages;
    }

    public List<Message<?>> getSplittedMessagesByKey(String key) {
        if (!getSplittedMessages().containsKey(key)) {
            addListOfSplittedMessages(key);
        }
        return getSplittedMessages().get(key);
    }

    private void addMessage(String key, Message<?> message) {
        getSplittedMessagesByKey(key).add(message);
    }

    private void addListOfSplittedMessages(String key) {
        getSplittedMessages().put(key, (new ArrayList<Message<?>>()));
    }
}
