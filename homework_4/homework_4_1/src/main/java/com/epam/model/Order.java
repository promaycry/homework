package com.epam.model;

public class Order {

    private OrderState orderState;

    private int id;

    @Override
    public String toString() {
        return "Order{" +
                "orderState=" + orderState +
                ", id=" + id +
                '}';
    }

    public Order(String text){

    }

    public OrderState getOrderState() {
        return orderState;
    }

    public void setOrderState(OrderState orderState) {
        this.orderState = orderState;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Order(){

    }

}
