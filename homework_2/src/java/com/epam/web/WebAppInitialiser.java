package com.epam.web;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.context.support.GenericWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;



public class WebAppInitialiser implements WebApplicationInitializer {

	final String location = System.getProperty("java.io.tmpdir");
	private int MAX_UPLOAD_SIZE = 5 * 1024 * 1024;

	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		AnnotationConfigWebApplicationContext root =
				new AnnotationConfigWebApplicationContext();

		root.scan("com.epam");
		servletContext.addListener(new ContextLoaderListener(root));

		ServletRegistration.Dynamic appServlet =
				servletContext.addServlet("mvc", new DispatcherServlet(new GenericWebApplicationContext()));
		appServlet.setLoadOnStartup(1);
		appServlet.addMapping("/");
		appServlet.setInitParameter("throwExceptionIfNoHandlerFound", "true");
		MultipartConfigElement multipartConfigElement = new MultipartConfigElement(location,
				MAX_UPLOAD_SIZE, MAX_UPLOAD_SIZE * 2, MAX_UPLOAD_SIZE / 2);
		appServlet.setMultipartConfig(multipartConfigElement);
		servletContext.setInitParameter("javax.servlet.jsp.jstl.fmt.localizationContext","resources");
	}

}
