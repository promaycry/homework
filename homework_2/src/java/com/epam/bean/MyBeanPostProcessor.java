package com.epam.bean;

import com.epam.annotation.MethodInterceptor;
import com.epam.annotation.Time;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;

import java.util.stream.Stream;

@Component
public class MyBeanPostProcessor implements BeanPostProcessor {

    @Override
    public Object postProcessBeforeInitialization(Object o, String s) throws BeansException {
        return o;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (hasAnyMeasuredMethod(bean)) {
            return measuredProxy(bean);
        }
        return bean;
    }

    private Object measuredProxy(Object bean) {
        ProxyFactory proxyFactory = new ProxyFactory(bean);
        proxyFactory.addAdvice(new MethodInterceptor());
        return proxyFactory.getProxy();
    }

    private boolean hasAnyMeasuredMethod(Object bean) {
        return Stream
                .of(ReflectionUtils.getAllDeclaredMethods(bean.getClass()))
                .anyMatch(method -> AnnotationUtils.getAnnotation(method, Time.class) != null);
    }
}
