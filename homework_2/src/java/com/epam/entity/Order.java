package com.epam.entity;

import java.math.BigDecimal;


/**
 * Order entity
 *
 * @author Kaliuha Aleksei
 *
 */
public class Order extends Entity{

    private static final long serialVersionUID = 5692708766041889396L;

    private Exposition exposition;

    private User user;

    private boolean done;

    private BigDecimal cost;

    private Integer number;

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Exposition getExposition() {
        return exposition;
    }

    public void setExposition(Exposition exposition) {
        this.exposition = exposition;
    }

    @Override
    public String toString() {
        return getId().toString();
    }
}
