package com.epam.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Time {
    String DEFAULT_PERFORMANCE_LOGGER = "PERFORMANCE-LOGGER";

    String value() default DEFAULT_PERFORMANCE_LOGGER;
}
