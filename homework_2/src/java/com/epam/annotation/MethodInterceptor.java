package com.epam.annotation;


import org.aopalliance.intercept.MethodInvocation;
import org.apache.log4j.Logger;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.util.StopWatch;

import java.lang.reflect.Method;

public class MethodInterceptor implements org.aopalliance.intercept.MethodInterceptor
{
    @Override
    public Object invoke(MethodInvocation methodInvocation) throws Throwable {
        final Method method = methodInvocation.getMethod();
        final Time annotation = AnnotationUtils.getAnnotation(method, Time.class);
        return annotation == null
                ? methodInvocation.proceed()
                : proceedMeasured(methodInvocation, annotation.value());
    }


    private Object proceedMeasured(MethodInvocation invocation, String loggerName) throws Throwable {
        final Logger logger = Logger.getLogger(loggerName);
        final StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        try {
            return invocation.proceed();
        } finally {
            stopWatch.stop();
            logger.trace(
                    "Execution of "+resolveLogMethod(invocation)+" took "+ stopWatch.getTotalTimeMillis()+" ms");
        }
    }

    private String resolveLogMethod(MethodInvocation invocation) {
        return invocation.getMethod().getDeclaringClass().getCanonicalName() + "#" + invocation.getMethod().getName();
    }

}
