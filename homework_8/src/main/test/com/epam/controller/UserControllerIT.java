package com.epam.controller;

import com.epam.AbstractSpringTest;
import com.epam.entity.User;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

public class UserControllerIT extends AbstractSpringTest {

    @Test
    @Transactional
    public void testRegisterUser() throws Exception {
        mockMvc.perform(post("/register")
                .param("login", "thebroxe")
                .param("password", "password")
                .param("password_confirm", "password")
                .param("email", "thebroxe@gmail.com"))
                .andExpect(status()
                        .is3xxRedirection()).andExpect(view().name("redirect:/expositionList"));
    }

    @Test
    @Transactional
    public void testRegisterUserLoginAndEmailExist() throws Exception {
        mockMvc.perform(post("/register")
                .param("login", "admin")
                .param("password", "password")
                .param("password_confirm", "password")
                .param("email", "allhackrat@gmail.com"))
                .andExpect(status()
                        .isOk()).andExpect(view().name("jsp/view/errorPage"));
    }

    @Test
    @Transactional
    public void testRegisterUserLoginExist() throws Exception {
            mockMvc.perform(post("/register")
                    .param("login", "admin")
                    .param("password", "password")
                    .param("password_confirm", "password")
                    .param("email", "thebroxe@gmail.com"))
                    .andExpect(status()
                            .isOk()).andExpect(view().name("jsp/view/errorPage"));
    }

    @Test
    @Transactional
    public void testRegisterUserEmailExist() throws Exception {
            mockMvc.perform(post("/register")
                    .param("login", "newUser")
                    .param("password", "password")
                    .param("password_confirm", "password")
                    .param("email", "allhackrat@gmail.com"))
                    .andExpect(status()
                            .isOk()).andExpect(view().name("jsp/view/errorPage"));
    }

    @Test
    @Transactional
    public void testRegisterUserConfirmPassword() throws Exception {
    mockMvc.perform(post("/register")
                .param("login", "newUser")
                .param("password", "password")
                .param("password_confirm", "nopassword")
                .param("email", "allhackrat@gmail.com"))
                .andExpect(status()
                        .isOk()).andExpect(view().name("jsp/view/errorPage"))
                .andExpect(model().attribute("errorMessage", "password.confirm"));
    }

    @Test
    @Transactional
    public void testRegisterUserLoginEmpty() throws Exception {
        mockMvc.perform(post("/register")
                .param("login", "")
                .param("password", "password")
                .param("password_confirm", "password")
                .param("email", "new@gmail.com"))
                .andExpect(status()
                        .isOk()).andExpect(view().name("jsp/view/errorPage"))
                .andExpect(model().attribute("errorMessage", "email.login.password.empty"));
    }

    @Test
    @Transactional
    public void testRegisterUserEmptyPassword() throws Exception {
        mockMvc.perform(post("/register")
                .param("login", "newUser")
                .param("password", "")
                .param("password_confirm", "password")
                .param("email", "new@gmail.com"))
                .andExpect(status()
                        .isOk()).andExpect(view().name("jsp/view/errorPage")).andExpect(model()
                .attribute("errorMessage", "email.login.password.empty"));
    }

    @Test
    @Transactional
    public void testRegisterUserEmptyPasswordConfirm() throws Exception {
        mockMvc.perform(post("/register")
                .param("login", "newUser")
                .param("password", "password")
                .param("password_confirm", "")
                .param("email", "new@gmail.com"))
                .andExpect(status()
                        .isOk()).andExpect(view().name("jsp/view/errorPage"))
                .andExpect(model().attribute("errorMessage", "email.login.password.empty"));
    }

    @Test
    @Transactional
    public void testRegisterUserEmptyEmail() throws Exception {
        mockMvc.perform(post("/register")
                .param("login", "newUser")
                .param("password", "password")
                .param("password_confirm", "password")
                .param("email", ""))
                .andExpect(status()
                        .isOk()).andExpect(view().name("jsp/view/errorPage"))
                .andExpect(model().attribute("errorMessage", "email.login.password.empty"));
    }

    @Test
    @Transactional
    public void testRegisterUserInvalidEmail() throws Exception {
        mockMvc.perform(post("/register")
                .param("login","newUser")
                .param("password","password")
                .param("password_confirm","password")
                .param("email","asdasdaasdasd"))
                .andExpect(status()
                        .isOk()).andExpect(view().name("jsp/view/errorPage"))
                .andExpect(model().attribute("errorMessage","email.invalid"));

    }

    @Test
    public void testGetUsers() throws Exception {
        mockMvc.perform(post("/users").with(user("admin").password("admin").roles("ADMIN")))
                .andExpect(status().
                        isOk()).
                andExpect(model().
                        attributeExists("userList","orderBy","currentPage","noOfPages")).
                andExpect(view().name("jsp/view/listOfUsers"));
    }

    @Test
    @Transactional
    public void testUpdateUserRole() throws Exception {
        mockMvc.perform(post("/update")
                .param("user", "2").with(user("admin").password("admin").roles("ADMIN")))
                .andExpect(view().name("redirect:/users?orderBy=default&currentPage=1"))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @Transactional
    public void testUserUpdateMoneyWithOutCardHolderName() throws Exception {
        User user=new User();
        user.setId((long)2);
        user.setMoney(new BigDecimal(300));
        mockMvc.perform(post("/money").sessionAttr("user",user)
                .param("money","300")
                .param("MM","1")
                .param("YY","20")
                .param("userName","")
                .param("numberCard","9999999999999999")
                .param("cvv","404")
                .with(user("user")
                        .password("user")
                        .roles("USER")))
                .andExpect(status().isOk())
                .andExpect(view().name("jsp/view/errorPage"));
    }

    @Test
    @Transactional
    public void testUserUpdateMoneyWithOutCardNumber() throws Exception {
        User user=new User();
        user.setId((long)2);
        user.setMoney(new BigDecimal(300));
        mockMvc.perform(post("/money").sessionAttr("user",user)
                .param("money","300")
                .param("MM","1")
                .param("YY","20")
                .param("userName","user")
                .param("numberCard","")
                .param("cvv","404")
                .with(user("user")
                        .password("user")
                        .roles("USER")))
                .andExpect(status().isOk())
                .andExpect(view().name("jsp/view/errorPage"));
    }

    @Test
    @Transactional
    public void testUserUpdateMoneyWhileOnOrders() throws Exception {
        User user=new User();
        user.setId((long)2);
        user.setMoney(new BigDecimal(300));
        mockMvc.perform(post("/money").sessionAttr("user",user)
                .param("money","300")
                .param("MM","1")
                .param("YY","20")
                .param("currentView","orders")
                .param("userName","user")
                .param("numberCard","9999999999999999")
                .param("cvv","404")
                .with(user("user")
                        .password("user")
                        .roles("USER")))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/orders?currentPage=1&orderBy=default"));
    }

    @Test
    @Transactional
    public void testUserUpdateMoneyWhileOnOrdersWithAnotherPage() throws Exception {
        User user=new User();
        user.setId((long)2);
        user.setMoney(new BigDecimal(300));
        mockMvc.perform(post("/money").sessionAttr("user",user)
                .param("money","300")
                .param("MM","1")
                .param("YY","20")
                .param("currentView","orders")
                .param("currentPage","2")
                .param("userName","user")
                .param("numberCard","9999999999999999")
                .param("cvv","404")
                .with(user("user")
                        .password("user")
                        .roles("USER")))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/orders?currentPage=2&orderBy=default"));
    }

    @Test
    @Transactional
    public void testUserUpdateMoneyWhileOnOrdersWithAnotherOrder() throws Exception {
        User user=new User();
        user.setId((long)2);
        user.setMoney(new BigDecimal(300));
        mockMvc.perform(post("/money").sessionAttr("user",user)
                .param("money","300")
                .param("MM","1")
                .param("YY","20")
                .param("currentView","orders")
                .param("orderBy","topicAsc")
                .param("userName","user")
                .param("numberCard","9999999999999999")
                .param("cvv","404")
                .with(user("user")
                        .password("user")
                        .roles("USER")))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/orders?currentPage=1&orderBy=topicAsc"));
    }

    @Test
    @Transactional
    public void testUserUpdateMoneyWhileOnOrdersWithAnotherOrderAndPage() throws Exception {
        User user=new User();
        user.setId((long)2);
        user.setMoney(new BigDecimal(300));
        mockMvc.perform(post("/money").sessionAttr("user",user)
                .param("money","300")
                .param("MM","1")
                .param("YY","20")
                .param("currentView","orders")
                .param("currentPage","2")
                .param("orderBy","topicAsc")
                .param("userName","user")
                .param("numberCard","9999999999999999")
                .param("cvv","404")
                .with(user("user")
                        .password("user")
                        .roles("USER")))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/orders?currentPage=2&orderBy=topicAsc"));
    }

    @Test
    @Transactional
    public void testUserUpdateMoneyWithBadCardNumber() throws Exception {
        User user=new User();
        user.setId((long)2);
        user.setMoney(new BigDecimal(300));
        mockMvc.perform(post("/money").sessionAttr("user",user)
                .param("money","300")
                .param("MM","1")
                .param("YY","20")
                .param("userName","user")
                .param("numberCard","666")
                .param("cvv","404")
                .with(user("user")
                        .password("user")
                        .roles("USER")))
                .andExpect(status().isOk())
                .andExpect(view().name("jsp/view/errorPage"));
    }


    @Test
    @Transactional
    public void testUserUpdateMoneyWithOutCVV() throws Exception {
        User user=new User();
        user.setId((long)2);
        user.setMoney(new BigDecimal(300));
        mockMvc.perform(post("/money").sessionAttr("user",user)
                .param("money","300")
                .param("MM","1")
                .param("YY","20")
                .param("userName","user")
                .param("numberCard","9999999999999999")
                .param("cvv","")
                .with(user("user")
                        .password("user")
                        .roles("USER")))
                .andExpect(status().isOk())
                .andExpect(view().name("jsp/view/errorPage"));
    }

    @Test
    @Transactional
    public void testUserUpdateMoneyWithOutMoney() throws Exception {
        User user=new User();
        user.setId((long)2);
        user.setMoney(new BigDecimal(300));
        mockMvc.perform(post("/money").sessionAttr("user",user)
                .param("money","")
                .param("MM","1")
                .param("YY","20")
                .param("userName","user")
                .param("numberCard","9999999999999999")
                .param("cvv","404")
                .with(user("user")
                        .password("user")
                        .roles("USER")))
                .andExpect(status().isOk())
                .andExpect(view().name("jsp/view/errorPage"));
    }

    @Test
    @Transactional
    public void testUserLogOut() throws Exception {
        User user=new User();
        user.setId((long)2);
        user.setMoney(new BigDecimal(300));
        MockHttpSession httpSession=new MockHttpSession();
        httpSession.setAttribute("user",user);
        mockMvc.perform(post("/logOut").session(httpSession)
                .with(user("user")
                        .password("user")
                        .roles("USER")))
                .andExpect(status().is3xxRedirection())
                .andExpect(view()
                        .name("redirect:/"));
        Assert.assertNull(httpSession.getAttribute("user"));
    }


    @Test
    @Transactional
    public void testUserUpdateMoneyWithOutMM() throws Exception {
        User user=new User();
        user.setId((long)2);
        user.setMoney(new BigDecimal(300));
        mockMvc.perform(post("/money").sessionAttr("user",user)
                .param("money","300")
                .param("MM","")
                .param("YY","20")
                .param("userName","user")
                .param("numberCard","9999999999999999")
                .param("cvv","404")
                .with(user("user")
                        .password("user")
                        .roles("USER")))
                .andExpect(status().isOk())
                .andExpect(view().name("jsp/view/errorPage"));
    }

    @Test
    @Transactional
    public void testUserUpdateMoneyWithOutYY() throws Exception {
        User user=new User();
        user.setId((long)2);
        user.setMoney(new BigDecimal(300));
        mockMvc.perform(post("/money").sessionAttr("user",user)
                .param("money","300")
                .param("MM","1")
                .param("YY","")
                .param("userName","user")
                .param("numberCard","9999999999999999")
                .param("cvv","404")
                .with(user("user")
                        .password("user")
                        .roles("USER")))
                .andExpect(status().isOk())
                .andExpect(view().name("jsp/view/errorPage"));
    }

    @Test
    @Transactional
    public void testUserUpdateMoney() throws Exception {
        User user=new User();
        user.setId((long)2);
        user.setMoney(new BigDecimal(300));
        mockMvc.perform(post("/money").sessionAttr("user",user)
                .param("money","300")
                .param("MM","1")
                .param("YY","20")
                .param("userName","user")
                .param("numberCard","9999999999999999")
                .param("cvv","404")
                .with(user("user")
                        .password("user")
                        .roles("USER")))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/expositionList?currentPage=1&sortBy=default"));
    }

    @Test
    @Transactional
    public void testUserUpdateMoneyAnotherPage() throws Exception {
        User user=new User();
        user.setId((long)2);
        user.setMoney(new BigDecimal(300));
        mockMvc.perform(post("/money").sessionAttr("user",user)
                .param("money","300")
                .param("MM","1")
                .param("YY","20")
                .param("userName","user")
                .param("currentPage","2")
                .param("numberCard","9999999999999999")
                .param("cvv","404")
                .with(user("user")
                        .password("user")
                        .roles("USER")))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/expositionList?currentPage=2&sortBy=default"));
    }

    @Test
    @Transactional
    public void testUserUpdateMoneyAnotherSort() throws Exception {
        User user=new User();
        user.setId((long)2);
        user.setMoney(new BigDecimal(300));
        mockMvc.perform(post("/money").sessionAttr("user",user)
                .param("money","300")
                .param("MM","1")
                .param("YY","20")
                .param("userName","user")
                .param("sortBy","topicAsc")
                .param("numberCard","9999999999999999")
                .param("cvv","404")
                .with(user("user")
                        .password("user")
                        .roles("USER")))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/expositionList?currentPage=1&sortBy=topicAsc"));
    }

    @Test
    @Transactional
    public void testUserUpdateMoneyAnotherSortAndPage() throws Exception {
        User user=new User();
        user.setId((long)2);
        user.setMoney(new BigDecimal(300));
        mockMvc.perform(post("/money").sessionAttr("user",user)
                .param("money","300")
                .param("MM","1")
                .param("YY","20")
                .param("userName","user")
                .param("sortBy","topicAsc")
                .param("currentPage","2")
                .param("numberCard","9999999999999999")
                .param("cvv","404")
                .with(user("user")
                        .password("user")
                        .roles("USER")))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/expositionList?currentPage=2&sortBy=topicAsc"));
    }

}
