package com.epam.controller;

import com.epam.dao.ExpositionDAO;
import com.epam.dao.LocaleDAO;
import com.epam.dao.OrderDAO;
import com.epam.dao.UserDAO;
import com.epam.entity.Exposition;
import com.epam.entity.Hall;
import com.epam.entity.Order;
import com.epam.entity.User;
import com.epam.util.OrderQuery;
import com.epam.web.controller.ErrorController;
import com.epam.web.controller.OrderController;
import com.epam.web.service.OrderService;
import org.aspectj.weaver.ast.Or;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

public class OrderControllerTest {

    UserDAO userDAO= Mockito.mock(UserDAO.class);

    OrderDAO orderDAO=Mockito.mock(OrderDAO.class);

    ExpositionDAO expositionDAO=Mockito.mock(ExpositionDAO.class);

    LocaleDAO localeDAO=Mockito.mock(LocaleDAO.class);

    OrderService orderService=new OrderService(userDAO,orderDAO,expositionDAO,localeDAO);

    private final OrderController orderController=new OrderController(orderService);

    private final MockMvc mockMvc= MockMvcBuilders
            .standaloneSetup(orderController)
            .setControllerAdvice(new ErrorController()).alwaysDo(print()).build();

    @Test
    public void testDeleteOrder() throws Exception {

        Order order=new Order();
        order.setId((long)1);

        User user=new User();
        user.setId((long)2);
        user.setMoney(new BigDecimal("300"));
        user.setPassword("user");
        user.setEmail("user@gmail.com");
        user.setLogin("user");
        user.setRole("ROLE_USER");

        User user1 =new User();
        user1.setId((long)1);
        user1.setMoney(new BigDecimal("0"));
        user1.setPassword("user");
        user1.setEmail("user@gmail.com");
        user1.setLogin("user");
        user1.setRole("ROLE_USER");

        BDDMockito.doNothing().when(orderDAO).delete(order);
        BDDMockito.when(userDAO.getByID((long)1)).thenReturn(user);

        MockHttpSession mockHttpSession=new MockHttpSession();
        mockHttpSession.setAttribute("user",user1);


        mockMvc.perform(post("/deleteOrder")
                .session(mockHttpSession)
                .param("order","1"))
                .andExpect(status().is3xxRedirection());

        Assert.assertEquals(new BigDecimal("300"),((User)mockHttpSession.getAttribute("user")).getMoney());

    }

    @Test
    public void testAddOrder() throws Exception {

        User user=new User();
        user.setId((long)2);
        user.setMoney(new BigDecimal("300"));
        user.setPassword("user");
        user.setEmail("user@gmail.com");
        user.setLogin("user");
        user.setRole("ROLE_USER");

        User user1=new User();
        user1.setId((long)2);
        user1.setMoney(new BigDecimal("0"));
        user1.setPassword("user");
        user1.setEmail("user@gmail.com");
        user1.setLogin("user");
        user1.setRole("ROLE_USER");

        Exposition exposition=new Exposition();

        exposition.setId((long)1);
        exposition.setCost(new BigDecimal("100"));
        exposition.setDate(java.sql.Date.valueOf(LocalDate.now()));
        exposition.setTicketsNum(3);
        exposition.setTopic("topic");

        Hall hall=new Hall();
        hall.setId((long)1);
        Hall hall1=new Hall();
        hall1.setId((long)2);

        exposition.getHalls().addAll(Arrays.asList(hall,hall1));
        exposition.setVisits(0);

        BDDMockito.when(localeDAO.getIdLocale("en")).thenReturn(1);

        BDDMockito.when(userDAO.getByID((long)2)).thenReturn(user1);

        BDDMockito.when(expositionDAO.getByID(exposition.getId(),1)).thenReturn(exposition);

        MockHttpSession mockHttpSession=new MockHttpSession();

        mockHttpSession.setAttribute("user",user);
        mockHttpSession.setAttribute("defaultLocale","en");

        MockHttpSession mockHttpSession1=new MockHttpSession();

        mockHttpSession1.setAttribute("user",user1);
        mockHttpSession1.setAttribute("defaultLocale","en");

        mockMvc.perform(post("/buy")
                .param("exposition_id","1")
                .param("number","3")
                .session(mockHttpSession))
                .andExpect(status().is3xxRedirection())
                .andExpect(view()
                .name("redirect:/expositionList?sortBy=default&currentPage=1"));

        mockMvc.perform(post("/buy")
                .param("exposition_id","1")
                .param("number","3")
                .session(mockHttpSession1))
                .andExpect(status().isOk())
                .andExpect(view()
                        .name("jsp/view/errorPage"));

        Assert.assertEquals(new BigDecimal("0"),((User)mockHttpSession.getAttribute("user")).getMoney());

    }

    @Test
    public void testGetOrders() throws Exception {

        User user=new User();
        user.setId((long)1);
        BDDMockito.when(orderDAO.getCount(user)).thenReturn(2);
        String query= OrderQuery.get("default");

        Order order=new Order();
        order.setId((long)1);

        Order order1=new Order();
        order1.setId((long)2);

        MockHttpSession mockHttpSession=new MockHttpSession();

        mockHttpSession.setAttribute("user",user);
        mockHttpSession.setAttribute("defaultLocale","en");

        List<Order>orders=Arrays.asList(order,order1);

        BDDMockito.when(orderDAO.getUserOrders(user,"en",1,6,query)).thenReturn(orders);

        mockMvc.perform(post("/orders")
                .session(mockHttpSession))
                .andExpect(status().isOk())
                .andExpect(view().name("jsp/view/bucket"))
                .andExpect(model().attribute("orderList",orders));

    }

}
