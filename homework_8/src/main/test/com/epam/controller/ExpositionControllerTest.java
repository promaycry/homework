package com.epam.controller;

import com.epam.dao.ExpositionDAO;
import com.epam.dao.LocaleDAO;
import com.epam.dao.UserDAO;
import com.epam.entity.Exposition;
import com.epam.entity.Hall;
import com.epam.entity.User;
import com.epam.util.ExpositionsQuery;
import com.epam.web.controller.ErrorController;
import com.epam.web.controller.ExpositionController;
import com.epam.web.service.ExpositionService;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;


public class ExpositionControllerTest {

    ExpositionDAO expositionDAO= Mockito.mock(ExpositionDAO.class);

    UserDAO userDAO= Mockito.mock(UserDAO.class);

    LocaleDAO localeDAO=Mockito.mock(LocaleDAO.class);

    ExpositionService expositionService=new ExpositionService(expositionDAO,userDAO,localeDAO);

    ExpositionController expositionController=new ExpositionController(expositionService);

    MockMvc mockMvc=MockMvcBuilders.standaloneSetup(expositionController).setControllerAdvice(new ErrorController()).alwaysDo(print()).build();

    static User user=new User();

    static List<Exposition>expositions=new ArrayList<>();

    static {
        user.setId((long)1);
        user.setPassword("password");
        user.setRole("ROLE_USER");
        user.setLogin("user");
        user.setEmail("user@gmail.com");
        user.setMoney(new BigDecimal("300"));
        Exposition exposition=new Exposition();
        exposition.setTopic("topic");
        exposition.setVisits(0);
        exposition.setTicketsNum(1);
        exposition.setId((long)1);
        exposition.setDate(Date.valueOf(LocalDate.now()));
        exposition.setCost(new BigDecimal("30"));
        exposition.getHalls().add(new Hall((long)1));
        expositions.add(exposition);
        Exposition exposition1=new Exposition();
        exposition1.setTopic("topic");
        exposition1.setVisits(0);
        exposition1.setTicketsNum(1);
        exposition1.setId((long)2);
        exposition1.setDate(Date.valueOf(LocalDate.now()));
        exposition1.setCost(new BigDecimal("30"));
        exposition1.getHalls().add(new Hall((long)2));
        expositions.add(exposition1);
    }

    @Test
    public void testGetExpositions() throws Exception {

        MockHttpSession mockHttpSession=new MockHttpSession();
        mockHttpSession.setAttribute("defaultLocale","en");
        mockHttpSession.setAttribute("user",user);

        BDDMockito.when(userDAO.getByID((long)1)).thenReturn(user);

        BDDMockito.when(expositionDAO.getCount(true)).thenReturn(2);
        BDDMockito.when(expositionDAO.getCount(false)).thenReturn(2);

        BDDMockito
                .when(expositionDAO
                        .getAll(anyString()
                                ,anyString()
                                ,eq(1)
                                ,eq(6)))
                .thenReturn(expositions);

        mockMvc.perform(post("/expositionList")
                .session(mockHttpSession))
                .andExpect(status().isOk())
                .andExpect(view().name("jsp/view/expositionList"))
        .andExpect(model().attribute("expositionList",expositions));

    }


    @Test
    public void testAddExposition() throws Exception {

        FileInputStream fi = new FileInputStream(new File("download (1).jpg"));
        MockMultipartFile multipartFile=new MockMultipartFile("image","download(1).jpg","multipart/form-data",fi);

        BDDMockito.doNothing().when(expositionDAO)
                .add(any(Exposition.class)
                ,any(HashMap.class)
                ,any(InputStream.class));

        BDDMockito.when(expositionDAO.getByTopic(anyString())).thenReturn(false);

        List<String>locales= Arrays.asList("en","ru","ua");

        BDDMockito.when(localeDAO.getLocales()).thenReturn(locales);

        mockMvc.perform(MockMvcRequestBuilders.multipart("/add").file(multipartFile).param("tickets","10")
                .param("price","30.03")
                .param("date","2021-12-13")
                .param("name_ru","�������")
                .param("name_en","english")
                .param("name_ua","���������")
                .param("halls","1","2","4"))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    public void testDeleteExposition() throws Exception {
        BDDMockito.doNothing().when(expositionDAO).delete(any(Exposition.class));
        mockMvc.perform(post("/delete").param("exposition_id","1"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/expositionList?sortBy=default&currentPage=1"));
    }

}
