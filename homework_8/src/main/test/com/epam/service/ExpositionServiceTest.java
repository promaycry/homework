package com.epam.service;

import com.epam.dao.ExpositionDAO;
import com.epam.dao.LocaleDAO;
import com.epam.dao.UserDAO;
import com.epam.entity.Exposition;
import com.epam.entity.Hall;
import com.epam.entity.User;
import com.epam.web.service.ExpositionService;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.http.HttpRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.ui.Model;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.ArgumentMatchers.any;

public class ExpositionServiceTest {

    ExpositionDAO expositionDAO= Mockito.mock(ExpositionDAO.class);

    UserDAO userDAO= Mockito.mock(UserDAO.class);

    LocaleDAO localeDAO=Mockito.mock(LocaleDAO.class);

    ExpositionService expositionService=new ExpositionService(expositionDAO,userDAO,localeDAO);

    static User user=new User();

    static List<Exposition> expositions=new ArrayList<>();

    HttpServletResponse httpServletResponse=Mockito.mock(HttpServletResponse.class);

    static {
        user.setId((long)1);
        user.setPassword("password");
        user.setRole("ROLE_USER");
        user.setLogin("user");
        user.setEmail("user@gmail.com");
        user.setMoney(new BigDecimal("300"));
        Exposition exposition=new Exposition();
        exposition.setTopic("topic");
        exposition.setVisits(0);
        exposition.setTicketsNum(1);
        exposition.setId((long)1);
        exposition.setDate(Date.valueOf(LocalDate.now()));
        exposition.setCost(new BigDecimal("30"));
        exposition.getHalls().add(new Hall((long)1));
        expositions.add(exposition);
        Exposition exposition1=new Exposition();
        exposition1.setTopic("topic");
        exposition1.setVisits(0);
        exposition1.setTicketsNum(1);
        exposition1.setId((long)2);
        exposition1.setDate(Date.valueOf(LocalDate.now()));
        exposition1.setCost(new BigDecimal("30"));
        exposition1.getHalls().add(new Hall((long)2));
        expositions.add(exposition1);
    }


    @Test
    public void testDeleteExposition() {
        BDDMockito.doNothing().when(expositionDAO).delete(any(Exposition.class));

        Assert.assertEquals("redirect:/expositionList?sortBy=default&currentPage=1",expositionService
                .deleteExposition(1, 1, "default"));

        Assert.assertEquals("redirect:/expositionList?sortBy=default&currentPage=2",expositionService
                                .deleteExposition(1, 2, "default"));

        Assert.assertEquals("redirect:/expositionList?sortBy=topicAsc&currentPage=1",expositionService
                                .deleteExposition(1, 1, "topicAsc"));

        Assert.assertEquals("redirect:/expositionList?sortBy=topicAsc&currentPage=2",expositionService
                                .deleteExposition(1, 2, "topicAsc"));
    }


    @Test
    public void testGetExpositions() {

        MockHttpSession mockHttpSession=new MockHttpSession();
        mockHttpSession.setAttribute("defaultLocale","en");
        mockHttpSession.setAttribute("user",user);

        BDDMockito.when(userDAO.getByID((long)1)).thenReturn(user);

        BDDMockito.when(expositionDAO.getCount(true)).thenReturn(2);
        BDDMockito.when(expositionDAO.getCount(false)).thenReturn(2);

        BDDMockito
                .when(expositionDAO
                        .getAll(anyString()
                                ,anyString()
                                ,anyInt()
                                ,eq(6)))
                .thenReturn(expositions);

        Model model=Mockito.mock(Model.class);

        Assert.assertEquals("jsp/view/expositionList", expositionService
                .get(mockHttpSession,1,"default",model));

        Assert.assertEquals("jsp/view/expositionList", expositionService
                .get(mockHttpSession,2,"default",model));

        Assert.assertEquals("jsp/view/expositionList", expositionService
                .get(mockHttpSession,1,"topicAsc",model));

        Assert.assertEquals("jsp/view/expositionList", expositionService
                .get(mockHttpSession,2,"topicAsc",model));
    }

    @Test
    public void testAddExposition() throws Exception {

        FileInputStream fi = new FileInputStream(new File("download (1).jpg"));
        MockMultipartFile multipartFile=new MockMultipartFile("image","download(1).jpg","multipart/form-data",fi);

        BDDMockito.doNothing().when(expositionDAO)
                .add(any(Exposition.class)
                        ,any(HashMap.class)
                        ,any(InputStream.class));

        BDDMockito.when(expositionDAO.getByTopic(anyString())).thenReturn(false);

        List<String>locales= Arrays.asList("en","ru","ua");

        BDDMockito.when(localeDAO.getLocales()).thenReturn(locales);

        HttpServletRequest httpRequest=Mockito.mock(HttpServletRequest.class);

        BDDMockito.when(httpRequest.getParameter("tickets")).thenReturn("10");

        BDDMockito.when(httpRequest.getParameter("price")).thenReturn("30.03");
        BDDMockito.when(httpRequest.getParameter("date")).thenReturn("2021-12-13");
        BDDMockito.when(httpRequest.getParameter("name_ru")).thenReturn("�������");
        BDDMockito.when(httpRequest.getParameter("name_en")).thenReturn("english");
        BDDMockito.when(httpRequest.getParameter("name_ua")).thenReturn("���������");
        BDDMockito.when(httpRequest.getParameterValues("halls")).thenReturn(new String[]{"1","2","4"});
        Assert.assertEquals("redirect:/addExposition",expositionService.add(httpServletResponse,httpRequest,multipartFile));

    }

    @Test
    public void testAddExpositionWithOutName() throws IOException {

        FileInputStream fi = new FileInputStream(new File("download (1).jpg"));
        MockMultipartFile multipartFile=new MockMultipartFile("image","download(1).jpg","multipart/form-data",fi);

        BDDMockito.doNothing().when(expositionDAO)
                .add(any(Exposition.class)
                        ,any(HashMap.class)
                        ,any(InputStream.class));

        BDDMockito.when(expositionDAO.getByTopic(anyString())).thenReturn(false);

        List<String>locales= Arrays.asList("en","ru","ua");

        BDDMockito.when(localeDAO.getLocales()).thenReturn(locales);

        HttpServletRequest httpRequest1=Mockito.mock(HttpServletRequest.class);
        BDDMockito.when(httpRequest1.getParameter("tickets")).thenReturn("10");
        BDDMockito.when(httpRequest1.getParameter("price")).thenReturn("30.03");
        BDDMockito.when(httpRequest1.getParameter("date")).thenReturn("2021-12-13");
        BDDMockito.when(httpRequest1.getParameter("name_ru")).thenReturn("�������");
        BDDMockito.when(httpRequest1.getParameter("name_ua")).thenReturn("���������");
        BDDMockito.when(httpRequest1.getParameterValues("halls")).thenReturn(new String[]{"1","2","4"});
        Assert.assertEquals("jsp/view/errorPage",expositionService
                .add(httpServletResponse,httpRequest1,multipartFile));
    }

    @Test
    public void testAddExpositionWithOutNames() throws IOException {

        FileInputStream fi = new FileInputStream(new File("download (1).jpg"));
        MockMultipartFile multipartFile=new MockMultipartFile("image","download(1).jpg","multipart/form-data",fi);

        BDDMockito.doNothing().when(expositionDAO)
                .add(any(Exposition.class)
                        ,any(HashMap.class)
                        ,any(InputStream.class));

        BDDMockito.when(expositionDAO.getByTopic(anyString())).thenReturn(false);

        List<String>locales= Arrays.asList("en","ru","ua");

        BDDMockito.when(localeDAO.getLocales()).thenReturn(locales);

        HttpServletRequest httpRequest1=Mockito.mock(HttpServletRequest.class);
        BDDMockito.when(httpRequest1.getParameter("tickets")).thenReturn("10");
        BDDMockito.when(httpRequest1.getParameter("price")).thenReturn("30.03");
        BDDMockito.when(httpRequest1.getParameter("date")).thenReturn("2021-12-13");
        BDDMockito.when(httpRequest1.getParameterValues("halls")).thenReturn(new String[]{"1","2","4"});
        Assert.assertEquals("jsp/view/errorPage",expositionService
                .add(httpServletResponse,httpRequest1,multipartFile));
    }


    @Test
    public void testAddExpositionWithOutDate() throws Exception {

        FileInputStream fi = new FileInputStream(new File("download (1).jpg"));
        MockMultipartFile multipartFile=new MockMultipartFile("image","download(1).jpg","multipart/form-data",fi);

        BDDMockito.doNothing().when(expositionDAO)
                .add(any(Exposition.class)
                        ,any(HashMap.class)
                        ,any(InputStream.class));

        BDDMockito.when(expositionDAO.getByTopic(anyString())).thenReturn(false);

        List<String>locales= Arrays.asList("en","ru","ua");

        BDDMockito.when(localeDAO.getLocales()).thenReturn(locales);

        HttpServletRequest httpRequest=Mockito.mock(HttpServletRequest.class);

        BDDMockito.when(httpRequest.getParameter("tickets")).thenReturn("10");

        BDDMockito.when(httpRequest.getParameter("price")).thenReturn("30.03");
        BDDMockito.when(httpRequest.getParameter("name_ru")).thenReturn("�������");
        BDDMockito.when(httpRequest.getParameter("name_en")).thenReturn("english");
        BDDMockito.when(httpRequest.getParameter("name_ua")).thenReturn("���������");
        BDDMockito.when(httpRequest.getParameterValues("halls")).thenReturn(new String[]{"1","2","4"});
        Assert.assertEquals("jsp/view/errorPage",
                expositionService.add(httpServletResponse,httpRequest,multipartFile));

    }


    @Test
    public void testAddExpositionWithOutTickets() throws Exception {

        FileInputStream fi = new FileInputStream(new File("download (1).jpg"));
        MockMultipartFile multipartFile=new MockMultipartFile("image","download(1).jpg","multipart/form-data",fi);

        BDDMockito.doNothing().when(expositionDAO)
                .add(any(Exposition.class)
                        ,any(HashMap.class)
                        ,any(InputStream.class));

        BDDMockito.when(expositionDAO.getByTopic(anyString())).thenReturn(false);

        List<String>locales= Arrays.asList("en","ru","ua");

        BDDMockito.when(localeDAO.getLocales()).thenReturn(locales);

        HttpServletRequest httpRequest=Mockito.mock(HttpServletRequest.class);

        BDDMockito.when(httpRequest.getParameter("price")).thenReturn("30.03");
        BDDMockito.when(httpRequest.getParameter("date")).thenReturn("2021-12-13");
        BDDMockito.when(httpRequest.getParameter("name_ru")).thenReturn("�������");
        BDDMockito.when(httpRequest.getParameter("name_en")).thenReturn("english");
        BDDMockito.when(httpRequest.getParameter("name_ua")).thenReturn("���������");
        BDDMockito.when(httpRequest.getParameterValues("halls")).thenReturn(new String[]{"1","2","4"});
        Assert.assertEquals("jsp/view/errorPage",expositionService.add(httpServletResponse,httpRequest,multipartFile));

    }


    @Test
    public void testAddExpositionWithOutPrice() throws Exception {

        FileInputStream fi = new FileInputStream(new File("download (1).jpg"));
        MockMultipartFile multipartFile=new MockMultipartFile("image","download(1).jpg","multipart/form-data",fi);

        BDDMockito.doNothing().when(expositionDAO)
                .add(any(Exposition.class)
                        ,any(HashMap.class)
                        ,any(InputStream.class));

        BDDMockito.when(expositionDAO.getByTopic(anyString())).thenReturn(false);

        List<String>locales= Arrays.asList("en","ru","ua");

        BDDMockito.when(localeDAO.getLocales()).thenReturn(locales);

        HttpServletRequest httpRequest=Mockito.mock(HttpServletRequest.class);

        BDDMockito.when(httpRequest.getParameter("tickets")).thenReturn("10");
        BDDMockito.when(httpRequest.getParameter("date")).thenReturn("2021-12-13");
        BDDMockito.when(httpRequest.getParameter("name_ru")).thenReturn("�������");
        BDDMockito.when(httpRequest.getParameter("name_en")).thenReturn("english");
        BDDMockito.when(httpRequest.getParameter("name_ua")).thenReturn("���������");
        BDDMockito.when(httpRequest.getParameterValues("halls")).thenReturn(new String[]{"1","2","4"});
        Assert.assertEquals("jsp/view/errorPage",
                expositionService.add(httpServletResponse,httpRequest,multipartFile));

    }


    @Test
    public void testAddExpositionWithOutHalls() throws Exception {

        FileInputStream fi = new FileInputStream(new File("download (1).jpg"));
        MockMultipartFile multipartFile=new MockMultipartFile("image","download(1).jpg","multipart/form-data",fi);

        BDDMockito.doNothing().when(expositionDAO)
                .add(any(Exposition.class)
                        ,any(HashMap.class)
                        ,any(InputStream.class));

        BDDMockito.when(expositionDAO.getByTopic(anyString())).thenReturn(false);

        List<String>locales= Arrays.asList("en","ru","ua");

        BDDMockito.when(localeDAO.getLocales()).thenReturn(locales);

        HttpServletRequest httpRequest=Mockito.mock(HttpServletRequest.class);

        BDDMockito.when(httpRequest.getParameter("tickets")).thenReturn("10");

        BDDMockito.when(httpRequest.getParameter("price")).thenReturn("30.03");
        BDDMockito.when(httpRequest.getParameter("date")).thenReturn("2021-12-13");
        BDDMockito.when(httpRequest.getParameter("name_ru")).thenReturn("�������");
        BDDMockito.when(httpRequest.getParameter("name_en")).thenReturn("english");
        BDDMockito.when(httpRequest.getParameter("name_ua")).thenReturn("���������");
        Assert.assertEquals("jsp/view/errorPage",
                expositionService.add(httpServletResponse,httpRequest,multipartFile));

    }

}
