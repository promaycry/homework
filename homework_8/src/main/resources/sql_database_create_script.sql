
CREATE TABLE  users_roles(
id INT primary key,
role_name VARCHAR(15) not null,
constraint users_roles_id check(id>=1)
);

INSERT INTO users_roles(id,role_name)
VALUES(1,'ROLE_ADMIN'),
(2,'ROLE_USER');

CREATE TABLE  users(
role_id int REFERENCES users_roles(id) ,
id int auto_increment primary key,
login varchar(20) unique NOT null,
password varchar(100) NOT NULL,
email varchar(40) not null,
money decimal(13,2) not null default 0,
constraint check_money check(money>=0),
constraint check_email check(email regexp '^[a-zA-Z0-9]+@[a-z]+\.(com||ua||ru||net||org)$')
);

INSERT INTO users(login,password,role_id,email)
values('admin','$2a$10$TgGqWaqkO2KXfvvUVCglIeQmO/tue5j77ABEBnc8jfAh30LjMwLDm',1,'promaycrynewtelefon@gmail.com');
INSERT INTO users(login,password,role_id,money,email)
values('user','$2a$10$TgGqWaqkO2KXfvvUVCglIeQmO/tue5j77ABEBnc8jfAh30LjMwLDm',2,300,'allhackrat@gmail.com');

Create table  halls(
id int primary key,
CONSTRAINT halls_id CHECK(ID>=1)
);

INSERT INTO halls(id) values(1),(2),(3),(4);

Create table expositions(
id int primary key auto_increment,
date date not null,
tickets_num int default 1,
visits int not null default 0 check(visits>=0),
cost decimal(13,2) default 0,
image longblob not null,
constraint check_cost check(cost>=0),
constraint check_visits check(visits<=tickets_num)
);

Create table  exposition_halls(
exposition_id int ,
hall_id int,
foreign key(exposition_id) references expositions(id) On DELETE CASCADE,
foreign key(hall_id) references halls(id) ON DELETE CASCADE
);

create table  locales(
id int primary key,
name varchar(5)
);

Create table exposition_names(
exposition_id int ,
topic varchar(100) not null unique,
locale_id int ,
foreign key(locale_id) references locales(id) ,
foreign key(exposition_id) references expositions(id) on delete cascade
);

insert into  locales(id,name) values(1,'en'),(2,'ru'),(3,'ua');

Create table   orders(
id int auto_increment primary key,
exposition_id int references expositions(id),
cost decimal(13,2),
user_id int references users(id),
number_Of_Tickets int
);

Create view get_Expositions_for_user as
Select e.id,e.tickets_num,e.date,e.visits,en.topic,en.locale_id,e.cost ,e.image
from expositions e
join exposition_names en
where en.exposition_id=e.id and e.date >=curdate() and e.visits<tickets_num;

Create view get_Expositions as
Select e.id,e.tickets_num,e.date,e.visits,en.topic,en.locale_id,e.cost,e.image
from expositions e
join exposition_names en
where en.exposition_id=e.id and date>=curdate()
;

Create view get_Expositions_for_orders as
Select e.id,e.tickets_num,e.date,e.visits,en.topic,en.locale_id,e.cost,e.image
from expositions e
join exposition_names en
where en.exposition_id=e.id;

