package com.epam.dao;

import com.epam.dao.mapper.OrderMapper;
import com.epam.entity.Exposition;
import com.epam.entity.Order;
import com.epam.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.epam.dao.DBManager.commitAndClose;
import static com.epam.dao.DBManager.rollbackAndClose;


/**
 * Data access object for order entity.
 */

@Repository
public class OrderDAO {

    private static final String SQL_ADD_ORDER="INSERT INTO orders(user_id,exposition_id,number_of_tickets) values(?,?,?)";

    private static final String SQL_UPDATE_ORDER="UPDATE orders set number_of_tickets=? where exposition_id=? and user_id=?";

    private static final String SQL_DELETE_ORDER="DELETE FROM orders WHERE id=?";

    private static final String SQL_GET_ORDER="SELECT * FROM get_orders_for_user where exposition_id=? and user_id=?";

    private static final String SQL_DELETE_ORDERS="DELETE FROM orders where exposition_id=?";

    private static final String SQL_GET_ORDER_BY_ID="Select * from get_orders_for_user where id=?";

    private static final String SQL_GET_COUNT="Select count( distinct id) from get_orders_for_user where user_id=?";

    @Autowired
    DBManager dbManager;

    @Autowired
    OrderMapper orderMapper;
    /**
     * Insert order
     *
     * @param user used id
     *
     * @param exposition used id
     *
     */
    public boolean add(User user, Exposition exposition,Integer number){
        if(get(user,exposition)!=null){
            updateOrder(user,exposition,number);
            return true;
        }
        Connection con = null;
        boolean add=false;
        try {
            con = dbManager.getConnection();
            PreparedStatement pstmt = con.prepareStatement(SQL_ADD_ORDER);
            int k = 1;
            pstmt.setLong(k++,user.getId());
            pstmt.setLong(k++,exposition.getId());
            pstmt.setLong(k,number);
            pstmt.executeUpdate();
            pstmt.close();
            con.commit();
            add=true;
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
            add=false;
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
        return add;
    }

    /**
     * Return user orders count
     *
     * @param user used id
     *
     * @return count of user orders
     *
     */
    public int getCount(User user){
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        int count=0;
        try {
            con = dbManager.getConnection();
            pstmt = con.prepareStatement(SQL_GET_COUNT);
            pstmt.setLong(1,user.getId());
            rs = pstmt.executeQuery();
            if (rs.next()){
                count=rs.getInt(1);
            }
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
            rollbackAndClose(con);
        } finally {
            commitAndClose(con);
        }
        return count;
    }


    /**
     * Delete all order for that exposition
     *
     * @param exposition used id
     *
     *
     */
    public void deleteExp(Exposition exposition){
        Connection con = null;
        try {
            con = dbManager.getConnection();
            PreparedStatement pstmt = con.prepareStatement(SQL_DELETE_ORDERS);
            pstmt.setLong(1,exposition.getId());
            pstmt.executeUpdate();
            pstmt.close();
            con.commit();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
    }


    /**
     * Get order if exist
     *
     * @param id
     *
     * @return order
     */
    public Order getById(Long id){
        Order order=null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        OrderMapper orderMapper =new OrderMapper();
        try {
            con = dbManager.getConnection();
            pstmt = con.prepareStatement(SQL_GET_ORDER_BY_ID);
            pstmt.setLong(1,id);
            rs = pstmt.executeQuery();
            if (rs.next()){
                order=orderMapper.mapRow(rs);
            }
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
            rollbackAndClose(con);
        } finally {
            commitAndClose(con);
        }
        return order;
    }

    /**
     * Delete order
     *
     * @param order used id
     *
     *
     */
    public void delete(Order order){
        Connection con = null;
        try {
            order=getById(order.getId());
            con = dbManager.getConnection();
            PreparedStatement pstmt = con.prepareStatement(SQL_DELETE_ORDER);
            pstmt.setLong(1,order.getId());
            pstmt.executeUpdate();
            pstmt.close();
            con.commit();
            new ExpositionDAO().updateExposition(order.getNumber(),order.getExposition().getId());
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
    }


    /**
     * Get order by user and exposition id
     *
     * @param user used id
     *
     *@param exposition used id
     *
     *
     */
    public Order get(User user,Exposition exposition){
        Order order=null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        OrderMapper orderMapper =new OrderMapper();
        try {
            con = dbManager.getConnection();
            pstmt = con.prepareStatement(SQL_GET_ORDER);
            pstmt.setLong(1,exposition.getId());
            pstmt.setLong(2,user.getId());
            rs = pstmt.executeQuery();
            if (rs.next()){
                order=orderMapper.mapRow(rs);
            }
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
            rollbackAndClose(con);
        } finally {
            commitAndClose(con);
        }
        return order;
    }


    /**
     * Get orders by user id
     *
     * @param user used id
     *
     * @param locale which language will get
     *
     * @param current current page for pagination
     *
     * @param limit how much per page
     */
    public List<Order> getUserOrders(User user,String locale,int current,int limit,String query){
        int start = current * limit - limit;
        List<Order>orders=new ArrayList<>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            con = dbManager.getConnection();
            pstmt = con.prepareStatement(query);
            pstmt.setLong(1,user.getId());
            pstmt.setLong(2,new LocaleDAO().getIdLocale(locale));
            pstmt.setInt(3,start);
            pstmt.setInt(4,limit);
            rs = pstmt.executeQuery();
            while (rs.next()){
                orders.add(orderMapper.mapRow(rs));
            }
            rs.close();
            pstmt.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
            rollbackAndClose(con);
        } finally {
            commitAndClose(con);
        }
        return orders;
    }


    /**
     * Update order cost and number of buyed tickets
     *
     * @param user used id
     *
     * @param exposition used id
     *
     * @param number  how much will be added
     */
    public void updateOrder(User user,Exposition exposition,Integer number){
        Connection con = null;
        try {
            con = dbManager.getConnection();
            PreparedStatement pstmt = con.prepareStatement(SQL_UPDATE_ORDER);
            int k = 1;
            pstmt.setLong(k++,number);
            pstmt.setLong(k++,exposition.getId());
            pstmt.setLong(k,user.getId());
            pstmt.executeUpdate();
            pstmt.close();
            con.commit();
        } catch (SQLException ex) {
            DBManager.getInstance().rollbackAndClose(con);
            ex.printStackTrace();
        } finally {
            DBManager.getInstance().commitAndClose(con);
        }
    }

}
