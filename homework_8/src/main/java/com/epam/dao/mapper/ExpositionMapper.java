package com.epam.dao.mapper;

import com.epam.dao.Fields;
import com.epam.dao.HallDAO;
import com.epam.entity.Exposition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Extracts a exposition from the result set row.
 */

@Service
public class ExpositionMapper implements EntityMapper<Exposition> {

@Autowired
HallDAO hallDAO;

    @Override
    public Exposition mapRow(ResultSet rs) {
        try {
            Exposition exposition = new Exposition();
            exposition.setId(rs.getLong(Fields.ID));
            String topic=rs.getString(Fields.TOPIC);
            exposition.setTopic(topic.substring(0,topic.length()-3));
            exposition.setVisits(rs.getInt(Fields.VISITS));
            exposition.setCost(rs.getBigDecimal(Fields.COST));
            exposition.setTicketsNum(rs.getInt(Fields.TICKETS_NUM));
            exposition.setDate(rs.getDate(Fields.DATE));
            exposition.setImage(rs.getBlob(Fields.IMAGE));
            exposition.getHalls().addAll(hallDAO.getExpositionsHalls(exposition));
            return exposition;
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

}
