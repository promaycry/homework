package com.epam.util;

import java.util.HashMap;


/**
 * Get sql query for getting orders
 *
 * @author Kaliuha Aleksei
 *
 */
public class OrderQuery {

    private final static HashMap<String,String>query=new HashMap<>();

    static {

        query.put("costDec", "Select * from get_orders_for_user where user_id=? and locale_id=? order by cost desc limit ?,?");

        query.put("costAsc", "Select * from get_orders_for_user where user_id=? and locale_id=? order by cost limit ?,?");

        query.put("priceDec", "Select * from get_orders_for_user where user_id=? and locale_id=? order by (cost*number_Of_Tickets)" +
                " desc limit ?,?");

        query.put("priceAsc", "Select * from get_orders_for_user where user_id=? and locale_id=? order by (cost*number_Of_Tickets) " +
                "limit ?,?");

        query.put("dateDec","Select * from get_orders_for_user where user_id=? and locale_id=? order by date desc limit ?,?");

        query.put("dateAsc","Select * from get_orders_for_user where user_id=? and locale_id=? order by date limit ?,?");

        query.put("topicDec","Select * from get_orders_for_user where user_id=? and locale_id=? order by topic desc limit ?,?");

        query.put("topicAsc", "Select * from get_orders_for_user where user_id=? and locale_id=? order by topic limit ?,?");

        query.put("ticketsDec", "Select * from get_orders_for_user where user_id=? and locale_id=? order by number_Of_Tickets " +
                "desc limit ?,?");

        query.put("ticketsAsc","Select * from get_orders_for_user where user_id=? and locale_id=? order by number_Of_Tickets " +
                "limit ?,?");

        query.put("default","SELECT * FROM get_orders_for_user where user_id=? and locale_id=? limit ?,?");


    }
    /**
     * @param sort
     * Select type of sort
     *
     * @return
     * SQL query to get orders
     *
     *
     * @author Kaliuha Aleksei
     *
     */
    public static String get( String sort){
        if(!query.containsKey(sort)){
            return query.get("default");
        }
        return query.get(sort);
    }
}
