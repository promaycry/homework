package com.epam.web.provider;

import com.epam.dao.UserDAO;
import com.epam.entity.User;
import com.epam.util.CustomPasswordEncoder;
import com.epam.util.Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {


    @Autowired
    private UserDAO userDAO;

    private CustomPasswordEncoder customPasswordEncoder=new CustomPasswordEncoder();

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String name = authentication.getName();
        String password = authentication.getCredentials().toString();
        User user=userDAO.get(name);
        if(user!=null){
        if(customPasswordEncoder.matches(password,user.getPassword())){
            return new UsernamePasswordAuthenticationToken(
                    new org.springframework.security.core.userdetails.User(user.getLogin(),
                            user.getPassword(),
                            new ArrayList<GrantedAuthority>(Arrays.asList(new SimpleGrantedAuthority(user.getRole())))), passwordEncoder.encode(password),
                    new ArrayList<GrantedAuthority>(Arrays.asList(new SimpleGrantedAuthority(user.getRole()))) );
        }
        else{
            throw new UsernameNotFoundException("User "+name+" not found");
        }
        }
        else{
            return null;
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
