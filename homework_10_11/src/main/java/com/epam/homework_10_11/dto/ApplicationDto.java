package com.epam.homework_10_11.dto;

import lombok.Data;

import java.util.UUID;


@Data
public class ApplicationDto {

    private UUID id;

    private String name;

}
