package com.epam.homework_10_11.dto;

import com.epam.homework_10_11.entity.Country;
import lombok.Data;

import java.util.*;

@Data
public class CompanyDto {

    private UUID id;

    private String name;

    private List<ApplicationDto>products=new ArrayList<>();

    private List<DeveloperDto>developers=new ArrayList<>();

    private Country country;

}
