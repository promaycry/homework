package com.epam.homework_10_11.service;

import com.epam.homework_10_11.dao.CompanyDao;
import com.epam.homework_10_11.dto.CompanyDto;
import com.epam.homework_10_11.entity.Company;
import com.epam.homework_10_11.entity.Application;
import com.epam.homework_10_11.entity.Developer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@Transactional
public class CompanyServiceImpl implements CompanyService {

    @Autowired
    private CompanyAssembler companyAssembler;

    @Autowired
    private CompanyDao companyDao;

    @Override
    public CompanyDto create(CompanyDto dto) {
        Company company = companyAssembler.assemble(dto);
        companyDao.save(company);
        CompanyDto result = companyAssembler.assemble(company);
        return result;
    }

    @Override
    public CompanyDto get(String name) {
        Company company = companyDao.get(name);
        return companyAssembler.assemble(company);
    }

    @Override
    public CompanyDto get(UUID uuid) {
        Company company = companyDao.get(uuid);
        return companyAssembler.assemble(company);
    }

    @Override
    public void delete(UUID id) {
        companyDao.delete(id);
    }

    @Override
    @Transactional
    public CompanyDto update(CompanyDto companyDto) {
        Company updatedCompany = companyAssembler.assemble(companyDto);
        Company company = companyDao.get(companyDto.getId());
        company.setName(updatedCompany.getName());
        updateApplications(company.getApplications(), updatedCompany.getApplications());
        updateDevelopers(company.getDevelopers(),updatedCompany.getDevelopers());
        return get(updatedCompany.getName());
    }

    private void updateApplications(List<Application> persistentApplications, List<Application> newApplications) {
        Map<UUID, Application> stillExistentJourneys = newApplications
                .stream()
                .filter(j -> Objects.nonNull(j.getId()))
                .collect(Collectors.toMap(Application::getId, Function.identity()));

        List<Application> applicationsToAdd = newApplications
                .stream()
                .filter(j -> Objects.isNull(j.getId()))
                .collect(Collectors.toList());

        Iterator<Application> persistentIterator = persistentApplications.iterator();
        while (persistentIterator.hasNext()) {
            Application persistentJourney = persistentIterator.next();
            if (stillExistentJourneys.containsKey(persistentJourney.getId())) {
                Application updatedJourney = stillExistentJourneys.get(persistentJourney.getId());
                updateApplication(persistentJourney, updatedJourney);
            } else {
                persistentIterator.remove();
                persistentJourney.setCompany(null);
            }
        }
        persistentApplications.addAll(applicationsToAdd);
    }

    private void updateDevelopers(List<Developer> persistentDevelopers, List<Developer> newDevelopers) {
        Map<UUID, Developer> stillExistentDevelopers = newDevelopers
                .stream()
                .filter(j -> Objects.nonNull(j.getId()))
                .collect(Collectors.toMap(Developer::getId, Function.identity()));

        List<Developer> developersToAdd = newDevelopers
                .stream()
                .filter(j -> Objects.isNull(j.getId()))
                .collect(Collectors.toList());

        Iterator<Developer> persistentIterator = persistentDevelopers.iterator();
        while (persistentIterator.hasNext()) {
            Developer persistentDeveloper = persistentIterator.next();
            if (stillExistentDevelopers.containsKey(persistentDeveloper.getId())) {
                Developer updatedDeveloper = stillExistentDevelopers.get(persistentDeveloper.getId());
                updateDeveloper(persistentDeveloper, updatedDeveloper);
            } else {
                persistentIterator.remove();
                persistentDeveloper.setCompany(null);
            }
        }
        persistentDevelopers.addAll(developersToAdd);
    }

    private void updateApplication(Application persistentApplication, Application updatedApplication) {
        persistentApplication.setCompany(updatedApplication.getCompany());
        persistentApplication.setName(updatedApplication.getName());
    }

    private void updateDeveloper(Developer persistentDeveloper, Developer updatedDeveloper) {
        persistentDeveloper.setCompany(updatedDeveloper.getCompany());
        persistentDeveloper.setName(updatedDeveloper.getName());
    }

}
