package com.epam.homework_10_11.service;

import com.epam.homework_10_11.entity.Country;

import java.util.UUID;

public interface CountryService {

    Country create(Country country);

    Country get(UUID id);

    Country get(String name);
}
