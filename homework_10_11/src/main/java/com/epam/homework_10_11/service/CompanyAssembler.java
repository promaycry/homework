package com.epam.homework_10_11.service;

import com.epam.homework_10_11.dto.CompanyDto;
import com.epam.homework_10_11.entity.Company;

public interface CompanyAssembler {

    Company assemble(CompanyDto companyDto);

    CompanyDto assemble(Company company);

}
