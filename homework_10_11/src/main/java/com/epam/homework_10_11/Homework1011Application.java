package com.epam.homework_10_11;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;

@SpringBootApplication(exclude= HibernateJpaAutoConfiguration.class)
public class Homework1011Application {

    public static void main(String[] args) {
        SpringApplication.run(Homework1011Application.class, args);
    }

}
