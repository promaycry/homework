package com.epam.homework_10_11.dao;

import com.epam.homework_10_11.entity.Country;

import java.util.UUID;

public interface CountryDao {

    void create(Country country);

    Country get(UUID id);

    Country get(String name);
}
