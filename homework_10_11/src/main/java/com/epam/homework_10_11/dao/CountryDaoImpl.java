package com.epam.homework_10_11.dao;

import com.epam.homework_10_11.entity.Country;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Objects;
import java.util.UUID;

@Repository
public class CountryDaoImpl implements CountryDao{

    @Autowired
    private SessionFactory sessionFactory;

    @PersistenceContext
    private EntityManager manager;

    @Override
    @Transactional
    public void create(Country country) {
        Session session=sessionFactory.getCurrentSession();
        session.save(country);
    }

    @Override
    @Transactional
    public Country get(UUID id) {
        Session session=sessionFactory.getCurrentSession();
        Country country=session.get(Country.class,id);
        return Objects.requireNonNull(country,"Country doesn`t exist");
    }

    @Override
    @Transactional
    public Country get(String name) {
        Session session=sessionFactory.getCurrentSession();
        Object result=session.getNamedQuery("countryByName")
                .setParameter("name",name)
                .getSingleResult();
        return (Country) result;
    }

}
