package com.epam.homework_10_11.service;

import com.epam.homework_10_11.config.TestConfig;
import com.epam.homework_10_11.dao.CountryDao;
import com.epam.homework_10_11.dto.CompanyDto;
import com.epam.homework_10_11.dto.ApplicationDto;
import com.epam.homework_10_11.dto.DeveloperDto;
import com.epam.homework_10_11.entity.Company;
import com.epam.homework_10_11.entity.Country;
import com.epam.homework_10_11.entity.Developer;
import net.bytebuddy.utility.RandomString;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.event.annotation.BeforeTestClass;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class})
public class CompanyServiceTest {

    @Autowired
    private CompanyService companyService;

    @Autowired
    private CountryService countryService;

    @Autowired
    private CountryDao countryDao;

    private static final String[] COUNTRY_NAMES = {"Ukraine", "America","Japane" };

    @Before
    public void createCountries() {
        Stream.of(COUNTRY_NAMES)
                .forEach(name -> countryService.create(new Country(name)));
    }

    @Test
    @Transactional
    public void testCreateCompany() {
        CompanyDto companyDto=companyDto("Line Corporation","Japane",30,"Line");
        CompanyDto createdCompany = companyService.create(companyDto);
        Assert.assertEquals(companyDto.getProducts().size(), createdCompany.getProducts().size());
        Assert.assertEquals(companyDto.getCountry(),createdCompany.getCountry());
        Assert.assertEquals(companyDto.getName(),createdCompany.getName());
        Assert.assertEquals(companyDto.getDevelopers().size(),createdCompany.getDevelopers().size());
        Assert.assertNotNull(createdCompany.getId());
    }

    private List<DeveloperDto>developers(Integer number){
        List<DeveloperDto>developers=new ArrayList<>();
        for(int i=0;i<number;i++){
            DeveloperDto developer=new DeveloperDto();
            developer.setBirthDate(Date.valueOf(LocalDate.now()));
            developer.setName(RandomString.make(10));
            developers.add(developer);
        }
        return developers;
    }

    @Test
    @Transactional
    public void testGetCompany() {
        String name="Rada";
        CompanyDto companyDto=companyDto(name,"Ukraine",10,"Diya");
        CompanyDto createdCompany = companyService.create(companyDto);
        CompanyDto gettedCompany = companyService.get(name);
        Assert.assertEquals(createdCompany.getProducts().size(), gettedCompany.getProducts().size());
        Assert.assertEquals(createdCompany.getProducts(), gettedCompany.getProducts());
        Assert.assertEquals(createdCompany.getCountry(),gettedCompany.getCountry());
        Assert.assertEquals(createdCompany.getName(),gettedCompany.getName());
        Assert.assertEquals(createdCompany.getId(),gettedCompany.getId());
        Assert.assertEquals(createdCompany.getDevelopers().size(),gettedCompany.getDevelopers().size());
    }

    @Test
    @Transactional
    public void testDeleteCompany() {
        CompanyDto touristDto = companyDto("Amazon","America",100,"Twitch");
        CompanyDto createdTourist = companyService.create(touristDto);
        companyService.delete(createdTourist.getId());
        try {
            companyService.get(createdTourist.getId());
        }
        catch (Exception e){
            Assert.assertEquals("Company doesn`t exist",e.getMessage());
            return;
        }
        Assert.fail("Company still exist");
    }

    @Test
    @Transactional
    public void testUpdateCompany() {
        CompanyDto companyDto = companyDto("Google","America",
                40,"Gmail",
                "Youtube");
        CompanyDto createdCompany = companyService.create(companyDto);

        ApplicationDto product=createdCompany.getProducts().get(0);
        product.setName("Learn");
        ApplicationDto applicationDto =new ApplicationDto();
        applicationDto.setName("New Product");
        createdCompany.getProducts().add(applicationDto);
        String newName="GoogleCompany Company";
        createdCompany.setName(newName);
        DeveloperDto gettedDeveloper=createdCompany.getDevelopers().get(0);
        gettedDeveloper.setName("New Developer");
        DeveloperDto developerDto=new DeveloperDto();
        developerDto.setName("Aleksei Kaliuha");
        developerDto.setBirthDate(Date.valueOf(LocalDate.now()));
        createdCompany.getDevelopers().add(developerDto);
        CompanyDto updatedCompany=companyService.update(createdCompany);
        CompanyDto newCompany=companyService.get(newName);
        Assert.assertNotNull(newCompany);
        Assert.assertEquals(updatedCompany.getId(),newCompany.getId());
        Assert.assertNotEquals(companyDto.getDevelopers().size(),newCompany.getDevelopers().size());
        Assert.assertNotEquals(companyDto.getName(),updatedCompany.getName());
        Assert.assertEquals(createdCompany.getId(),updatedCompany.getId());
        Assert.assertNotEquals(companyDto.getProducts().size(),updatedCompany.getProducts().size());
        Assert.assertEquals(3,updatedCompany.getProducts().size());
        Assert.assertEquals(newName,updatedCompany.getName());
        Assert.assertEquals(companyDto.getCountry(),updatedCompany.getCountry());
    }

    private CompanyDto companyDto(String name,String country,Integer number,String...products){
        CompanyDto companyDto = new CompanyDto();
        companyDto.setName(name);
        companyDto.setCountry(countryDao.get(country));
        companyDto.setDevelopers(developers(number));
        for (String productName: products ) {
            companyDto.getProducts().add(country(productName));
        }
        return companyDto;
    }

    private ApplicationDto country(String name){
        ApplicationDto applicationDto =new ApplicationDto();
        applicationDto.setName(name);
        return applicationDto;
    }

}
