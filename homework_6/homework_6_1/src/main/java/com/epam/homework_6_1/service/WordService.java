package com.epam.homework_6_1.service;

import com.epam.homework_6_1.annotation.Time;

import java.util.Arrays;
import java.util.List;

public class WordService {

    private static final String PATTERN_SPLIT=" {1,}";

    private static final String PATTERN_REPLACE="[,.?!]";

    @Time
    public List<String> getWords(String line){
       return Arrays.asList(line.replaceAll(PATTERN_REPLACE," ").split(PATTERN_SPLIT));
    }

}
