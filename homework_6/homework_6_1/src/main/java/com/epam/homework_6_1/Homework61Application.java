package com.epam.homework_6_1;

import com.epam.homework_6_1.service.WordService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

@Slf4j
public class Homework61Application {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
       List<String> words= ((WordService) context.getBean("wordService")).
               getWords(new String("It`s allright,I just need few words, and all of us will come along."));
       log.info("Words finded--->"+words.toString());
       log.info("Count of words finded-->"+words.size());
    }

}
