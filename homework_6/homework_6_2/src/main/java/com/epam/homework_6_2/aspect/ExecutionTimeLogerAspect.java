package com.epam.homework_6_2.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Aspect
@Component
@EnableAspectJAutoProxy
public class ExecutionTimeLogerAspect
{
    @Around("@annotation(com.epam.homework_6_2.annotation.Time)")
    public Object logExecutionTime(ProceedingJoinPoint point) throws Throwable
    {
        long start = System.currentTimeMillis();
        Object result = point.proceed();
        long time=System.currentTimeMillis()-start;
        final Logger logger= LoggerFactory.getLogger(
                MethodSignature.class.cast(point.getSignature()).getDeclaringTypeName());
        logger.info(String.format("PERFOMANCE-LOGGER : Method: %s , execution time: %d ms",
                ((MethodSignature) point.getSignature()).getMethod().getName(),
                time));
        return result;
    }

    @Before("@annotation(com.epam.homework_6_2.annotation.Time)")
    public void before(JoinPoint jp){
        final Logger logger= LoggerFactory.getLogger(
                MethodSignature.class.cast(jp.getSignature()).getDeclaringTypeName());
        Object[]args=jp.getArgs();
        logger.info(String.format("%s start work with arguments: "+ Arrays.asList(args).toString(),
                ((MethodSignature) jp.getSignature()).getMethod().getName()));
    }

    @After("@annotation(com.epam.homework_6_2.annotation.Time)")
    public void after(JoinPoint jp){
        final Logger logger= LoggerFactory.getLogger(
                MethodSignature.class.cast(jp.getSignature()).getDeclaringTypeName());
        logger.info(String.format("%s finished work",
                ((MethodSignature) jp.getSignature()).getMethod().getName()));
    }

}
