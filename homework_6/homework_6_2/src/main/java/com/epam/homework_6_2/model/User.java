package com.epam.homework_6_2.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * User entity.
 *
 * @author Kaliuha Aleksei
 *
 */
@Entity
@Table(name = "users")
@Data
public class User {
    
    @javax.persistence.Id
    private Long id;

    private static final long serialVersionUID = -6889036256149495388L;

    @Column(name = "login")
    private String login;

    @Column(name = "email")
    private String email;

    @Column(name = "money")
    private BigDecimal money;

}
