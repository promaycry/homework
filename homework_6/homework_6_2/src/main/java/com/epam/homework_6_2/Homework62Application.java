package com.epam.homework_6_2;

import com.epam.homework_6_2.service.UserService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class Homework62Application {

    public static void main(String[] args) {
        ConfigurableApplicationContext context=SpringApplication.run(Homework62Application.class, args);
        context.getBean(UserService.class).getUsers();
    }

}
