package com.epam.web.filter;

import com.epam.entity.User;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Security filter.
 *
 * @author Kaliuha Aleksei
 *
 */

@WebFilter(urlPatterns = "/*", dispatcherTypes = {DispatcherType.REQUEST})
public class AccessFilter extends HttpFilter {

    private static final Logger log = Logger.getLogger(AccessFilter.class);

    List<String> loginUser=new ArrayList<>(Arrays.asList(
            "/orders","/buy","/deleteOrder","/logOut","/money"));

    List<String> adminUser=new ArrayList<>(Arrays.asList("/users","/update","/addExposition","/add","/delete","/logOut"));

    List<String> notLoginedUser=new ArrayList<>(Arrays.asList("/register","/logIn","/registerPage"));

    List<String> outOfControl=new ArrayList<>(Arrays.asList("/expositionList","/setting","/"));

    HashMap<String,List<String>>control=new HashMap<>();

    @Override
    protected void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        log.debug("Filter starts");
        HttpServletRequest httpRequest = request;
        log.trace("Request uri --> " + httpRequest.getRequestURI());
        log.trace("Action-->"+request.getServletPath());
        if (accessAllowed(request)) {
            chain.doFilter(request, response);
        } else {
            String errorMessage = "permissionFailed";

            request.setAttribute("errorMessage", errorMessage);

            log.trace("Set the request attribute: errorMessage --> " + errorMessage);

            request.getRequestDispatcher("/WEB-INF/jsp/view/errorPage.jsp")
                    .forward(request, response);
        }
    }

    private boolean accessAllowed(ServletRequest request) {
        HttpServletRequest httpRequest = (HttpServletRequest) request;

        String requestURL=httpRequest.getServletPath();

        log.trace("Reguest-->"+requestURL);
        if (requestURL.isEmpty())
            return true;

        if (outOfControl.contains(requestURL))
            return true;

        HttpSession session = httpRequest.getSession(false);
        if (session == null)
            return false;

        User user = (User)session.getAttribute("user");
        String role=null;
        if(user==null){
            log.trace("user is aunthorized");
            role="unAuthorized user";
        }
        else {
            role=user.getRole();
            log.trace("user is "+role);
        }

        return control.get(role).contains(requestURL);

    }


    public void init(FilterConfig fConfig) throws ServletException {
        log.debug("Access Filter initialization starts");
        control.put("admin",adminUser);
        control.put("authorized user",loginUser);
        control.put("unAuthorized user",notLoginedUser);
        log.debug("Access Filter initialization finished");
    }


    @Override
    public void destroy() {
        log.debug("Access Filter destruction starts");
        log.debug("Access Filter destruction finished");
    }


}

