<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: allha
  Date: 14.10.2020
  Time: 19:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="../css/loginPageStyle.css">
</head>
<body>
<div id="id04" class="modal">
    <aside class="col-sm-6">
        <article class="card">
            <div class="card-body p-5">
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="nav-tab-card">
                        <form role="form" action="money" method="post">
                            <div class="form-group">
                                <label><fmt:message key="Sum"/></label>
                                <input type="number" step="0.50" class="form-control" name="money" min="10" required>
                            </div>

                            <div class="form-group">
                                <label><fmt:message key="fullName"/></label>
                                <input type="text" class="form-control" name="userName" placeholder="" required>
                            </div> <!-- form-group.// -->

                            <div class="form-group">
                                <label ><fmt:message key="cardNumber"/></label>
                                <div class="input-group">
                                    <input type="number" class="form-control" name="numberCard" placeholder="" required minlength="16" maxlength="16">
                                    <div class="input-group-append">
				<span class="input-group-text text-muted">
					<i class="fab fa-cc-visa"></i>   <i class="fab fa-cc-amex"></i>  
					<i class="fab fa-cc-mastercard"></i>
				</span>
                                    </div>
                                </div>
                            </div> <!-- form-group.// -->

                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <label><span class="hidden-xs"><fmt:message key="Exparation"/></span></label>
                                        <div class="input-group">
                                            <input type="number" class="form-control" placeholder="MM" name="MM" max="12" min="1" required>
                                            <input type="number" class="form-control" placeholder="YY" name="YY" min="00" required>
                                            <input type="hidden" name="currentView" value="${currentView}">
                                            <input type="hidden" name="currentPage" value="${currentPage}">
                                            <input type="hidden" name="orderBy" value="${orderBy}">
                                            <input type="hidden" name="sortBy" value="${sortBy}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label data-toggle="tooltip" title="" data-original-title="3 digits code on back side of the card">CVV <i class="fa fa-question-circle"></i></label>
                                        <input type="password" class="form-control" name="cvv" required  minlength="3" maxlength="3">
                                    </div> <!-- form-group.// -->
                                </div>
                            </div> <!-- row.// -->
                            <button class="subscribe btn btn-primary btn-block" type="submit"> <fmt:message key="confirm.button.jsp"/>  </button>
                        </form>
                    </div>
                </div>

            </div>
        </article>
    </aside>
</div>
</body>
</html>
