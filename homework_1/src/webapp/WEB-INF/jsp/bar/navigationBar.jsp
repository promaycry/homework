<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
</head>
<body>
<nav class="navbar navbar-expand-sm navbar-dark bg-primary">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <form action="expositionList" method="post" id="goToList">
                <a onclick="document.getElementById('goToList').submit()" class="waves-effect"
                   style="color: black"><spring:message code="home.button.jsp"/></a>
            </form>
        </li>
        <li class="breadcrumb-item">
            <a onclick="document.getElementById('id03').style.display='block'" class="waves-effect"
               style="color: black"><fmt:message key="setting.button.jsp"/></a>
        </li>
    </ol>
    <div class="collapse navbar-collapse" id="basicExampleNav1">
        <ul class="navbar-nav ml-auto">
            <c:if test="${not empty user}">
            <c:choose>
            <c:when test="${user.role=='authorized user'}">
            <li class="nav-item">
                <form action="orders" method="post" id="goToBucket">
                    <a onclick="document.getElementById('goToBucket').submit()"
                       class="nav-link navbar-link-2 waves-effect"><i class="fas fa-shopping-cart pl-0"></i></a>
                </form>
            </li>
            </c:when>
            <c:when test="${user.role=='admin'}">
            <li class="nav-item">
                <form action="addExposition" method="post" id="goToPageAdd">
                    <input type="hidden" name="command" value="goToPageAdd">
                    <a onclick="document.getElementById('goToPageAdd').submit()"
                       class="nav-link navbar-link-2 waves-effect"><i class="fa fa-plus-square"></i></a>
                </form>
            </li>
                <li class="nav-item">
                    <form action="users" method="post" id="goToPageUsers">
                        <a onclick="document.getElementById('goToPageUsers').submit()"
                           class="nav-link navbar-link-2 waves-effect"><i class="fa fa-id-card"></i></a>
                    </form>
                </li>
            </c:when>
            </c:choose>
            </c:if>
            <c:if test="${not empty user}">
            <c:choose>
            <c:when test="${user.role=='authorized user'}">
            <li class="nav-item">
                <a onclick="document.getElementById('id04').style.display='block'"
                   class="nav-link navbar-link-2 waves-effect"><i class="fa fa-credit-card"></i></a>
            </li>
            </c:when>
            </c:choose>
            <li class="nav-item">
                <a class="nav-link waves-effect">${user.login} </a>
            </li>
            <c:choose>
            <c:when test="${user.role=='authorized user'}">
            <li class="nav-item">
                <a class="nav-link waves-effect">&nbsp;${user.money}$</a>
            </li>
            </c:when>
            </c:choose>

            </c:if>
            <c:if test="${empty user}">
            <li class="nav-item">
                <form action="registerPage" method="post" id="register">
                    <div class="form-inline">
                        <input type="hidden" name="command" value="goToPageRegister"/>
                        <a onclick="document.getElementById('register').submit()"
                           class="btn btn-outline-success my-2 my-sm-0"><fmt:message key="register.button.jsp"/></a>
                    </div>
                </form>
            </li>
            <li class="nav-item pl-2 mb-2 mb-md-0">
                <a onclick="document.getElementById('id01').style.display='block'"
                   class="btn btn-outline-info btn-md btn-rounded btn-navbar waves-effect waves-light"><fmt:message
                        key="signIn.button.jsp"/></a>
            </li>
    </div>

    </c:if>
    <c:if test="${not empty user}">
        <li class="nav-item pl-2 mb-2 mb-md-0">
            <a onclick="document.getElementById('id02').style.display='block'"
               class="btn btn-outline-info btn-md btn-rounded btn-navbar waves-effect waves-light"><fmt:message
                    key="logOut.button.jsp"/></a>
        </li>
    </c:if>
    </ul>
    </div>
</nav>

<jsp:include page="../modal/id01.jsp"/>

<jsp:include page="../modal/id02.jsp"/>

<div id="id03" class="modal">
    <form class="modal-content animate" id="setting" action="setting" method="post">
        <div class="imgcontainer">
            <span onclick="document.getElementById('id03').style.display='none'" class="close" title="Close Modal">&times;</span>
        </div>
        <br>
        <div class="container">
            <input type="hidden" name="command" value="setting"/>
            <c:if test="${not empty user}">
                <label><b><fmt:message key="newPassword.label"/></b></label>
                <input type="password" name="newPassword"/>
                <label><b><fmt:message key="oldPassword.label"/></b></label>
                <input type="password" name="oldPassword" />
                <label><b><fmt:message key="newEmail.label"/></b></label>
                <input type="text" name="newEmail">
                <label><b><fmt:message key="oldEmail.label"/></b></label>
                <input type="text" name="oldEmail">
            </c:if>
            <label><b><fmt:message key="selectLanguage.label"/></b></label>
            <select name="locale">
                <c:forEach items="${locales}" var="locale">
                    <c:if test="${locale==defaultLocale}">
                        <option selected="selected">${locale}</option>
                    </c:if>
                    <c:if test="${locale!=defaultLocale}">
                        <option>${locale}</option>
                    </c:if>
                </c:forEach>
            </select>
            <input type="hidden" name="currentView" value="${currentView}">
            <input type="hidden" name="currentPage" value="${currentPage}">
            <input type="hidden" name="orderBy" value="${orderBy}">
            <input type="hidden" name="sortBy" value="${sortBy}">
            <button type="submit"><fmt:message key="update.label"/></button>
        </div>
    </form>
</div>

<jsp:include page="../modal/id04.jsp"/>


<script>
    // Get the modal
    var modal = document.getElementById('id01');

    var modal1 = document.getElementById('id02');

    var modal2 = document.getElementById('id03');

    var modal3 = document.getElementById('id04');

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
        if (event.target == modal1) {
            modal1.style.display = "none";
        }
        if (event.target == modal2) {
            modal2.style.display = "none";
        }
        if (event.target == modal3) {
            modal3.style.display = "none";
        }
    }

</script>

</body>
</html>