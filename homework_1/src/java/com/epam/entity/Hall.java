package com.epam.entity;


/**
 * Hall entity
 *
 * @author Kaliuha Aleksei
 *
 */
public class Hall extends Entity{

    private static final long serialVersionUID = 2386302708905518585L;

    public Hall(){}
    public Hall(Long id){
        setId(id);
    }

    @Override
    public String toString() {
        return "Hall [id= "+getId()+" ]";
    }
}
