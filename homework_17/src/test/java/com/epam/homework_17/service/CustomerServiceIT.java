package com.epam.homework_17.service;

import com.epam.homework_17.Homework17Application;
import com.epam.homework_17.model.Account;
import com.epam.homework_17.model.Address;
import com.epam.homework_17.model.Customer;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Homework17Application.class)
@ActiveProfiles("test")
class CustomerServiceIT {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private MongoTemplate mongoTemplate;

    @AfterEach
    public void clearUp(){
        mongoTemplate.getDb().drop();
    }

    @Test
    public void testAddCustomer() {
        Customer customer=new Customer();
        customer.setFirstName("Alex");
        customer.setLastName("Merser");
        ArrayList<Account> accounts=new ArrayList<>();
        Account account=new Account();
        account.setCardNumber("1616161616161616");
        account.setExpirationDate(LocalDate.now());
        account.setNameOnAccount("Alex");
        accounts.add(account);
        ArrayList<Address>addresses=new ArrayList<>();
        Address address=new Address();
        address.setCountryCode(380);
        address.setLine1("Line1");
        address.setLine2("Line2");
        addresses.add(address);
        customer.setAccounts(accounts);
        customer.setAddresses(addresses);

        customerService.addCustomer(customer);

        Assert.assertNotNull(customer.getId());
    }

    @Test
    public void testGetCustomerByNames() {
        Customer customer=new Customer();
        customer.setFirstName("Alex");
        customer.setLastName("Merser");
        ArrayList<Account>accounts=new ArrayList<>();
        Account account=new Account();
        account.setCardNumber("1616161616161616");
        account.setExpirationDate(LocalDate.now());
        account.setNameOnAccount("Alex");
        accounts.add(account);
        ArrayList<Address>addresses=new ArrayList<>();
        Address address=new Address();
        address.setCountryCode(380);
        address.setLine1("Line1");
        address.setLine2("Line2");
        addresses.add(address);
        customer.setAccounts(accounts);
        customer.setAddresses(addresses);
        Customer secondcustomer=new Customer();
        secondcustomer.setFirstName("Oleksei");
        secondcustomer.setLastName("Kaliuha");
        ArrayList<Account>secondaccounts=new ArrayList<>();
        Account secondaccount=new Account();
        secondaccount.setCardNumber("1717171717171717");
        secondaccount.setExpirationDate(LocalDate.now());
        secondaccount.setNameOnAccount("Alex");
        secondaccounts.add(secondaccount);
        ArrayList<Address>secondaddresses=new ArrayList<>();
        Address secondaddress=new Address();
        secondaddress.setCountryCode(380);
        secondaddress.setLine1("Line1");
        secondaddress.setLine2("Line2");
        secondaddresses.add(secondaddress);
        secondcustomer.setAccounts(secondaccounts);
        secondcustomer.setAddresses(secondaddresses);

        customerService.addCustomer(customer);
        Customer gettedUser=customerService.getCustomerByFirstNameAndLastName("Alex","Merser");

        Assert.assertNotNull(gettedUser);
        Assert.assertEquals(customer.getAccounts().get(0).getCardNumber(),
                gettedUser.getAccounts().get(0).getCardNumber());
    }

    @Test
    public void testGetCustomerById() {
        Customer customer=new Customer();
        customer.setFirstName("Alex");
        customer.setLastName("Merser");
        ArrayList<Account>accounts=new ArrayList<>();
        Account account=new Account();
        account.setCardNumber("1616161616161616");
        account.setExpirationDate(LocalDate.now());
        account.setNameOnAccount("Alex");
        accounts.add(account);
        ArrayList<Address>addresses=new ArrayList<>();
        Address address=new Address();
        address.setCountryCode(380);
        address.setLine1("Line1");
        address.setLine2("Line2");
        addresses.add(address);
        customer.setAccounts(accounts);
        customer.setAddresses(addresses);

        customerService.addCustomer(customer);
        Customer gettedUser=customerService.getCustomer(customer.getId());

        Assert.assertNotNull(gettedUser);
        Assert.assertEquals(customer.getFirstName(),gettedUser.getFirstName());
        Assert.assertEquals(customer.getLastName(),gettedUser.getLastName());
    }

    @Test
    public void testUpdateCustomer(){
        Customer customer=new Customer();
        customer.setFirstName("Alex");
        customer.setLastName("Merser");
        ArrayList<Account>accounts=new ArrayList<>();
        Account account=new Account();
        account.setCardNumber("1616161616161616");
        account.setExpirationDate(LocalDate.now());
        account.setNameOnAccount("Alex");
        accounts.add(account);
        ArrayList<Address>addresses=new ArrayList<>();
        Address address=new Address();
        address.setCountryCode(380);
        address.setLine1("Line1");
        address.setLine2("Line2");
        addresses.add(address);
        customer.setAccounts(accounts);
        customer.setAddresses(addresses);

        customerService.addCustomer(customer);
        customer.setLastName("Exile");
        customerService.updateCustomer(customer);
        Customer customerFromBD=customerService.getCustomerByFirstNameAndLastName("Alex","Exile");

        Assert.assertNotNull(customerFromBD);
        Assert.assertEquals(customer.getId(),customerFromBD.getId());

    }

    @Test
    public void testGetCustomersWithExpiredCards() {
        Customer customer=new Customer();
        customer.setFirstName("Alex");
        customer.setLastName("Merser");
        List<Account> accounts=new ArrayList<>();
        Account account=new Account();
        account.setCardNumber("1616161616161616");
        account.setExpirationDate(LocalDate.now().minusDays(1));
        account.setNameOnAccount("Alex");
        accounts.add(account);
        ArrayList<Address>addresses=new ArrayList<>();
        Address address=new Address();
        address.setCountryCode(380);
        address.setLine1("Line1");
        address.setLine2("Line2");
        addresses.add(address);
        Customer secondCustomer=new Customer();
        secondCustomer.setFirstName("Olekseii");
        secondCustomer.setLastName("Kaliuha");
        List<Account>secondAccounts=new ArrayList<>();
        Account secondAccount=new Account();
        secondAccount.setCardNumber("1616161616161616");
        secondAccount.setExpirationDate(LocalDate.now().plusDays(4));
        secondAccount.setNameOnAccount("Alex");
        secondAccounts.add(secondAccount);
        ArrayList<Address>secondAddresses=new ArrayList<>();
        Address secondAddress=new Address();
        secondAddress.setCountryCode(380);
        secondAddress.setLine1("Line1");
        secondAddress.setLine2("Line2");
        secondAddresses.add(secondAddress);
        Customer thirdCustomer=new Customer();
        thirdCustomer.setFirstName("Olekseii");
        thirdCustomer.setLastName("Kaliuha");
        List<Account>thirdAccounts=new ArrayList<>();
        Account thirdAccount=new Account();
        thirdAccount.setCardNumber("1616161616161616");
        thirdAccount.setExpirationDate(LocalDate.now().plusDays(4));
        thirdAccount.setNameOnAccount("Alex");
        Account fourthAccount=new Account();
        fourthAccount.setCardNumber("1616161616161616");
        fourthAccount.setExpirationDate(LocalDate.now().minusDays(4));
        fourthAccount.setNameOnAccount("Alex");
        ArrayList<Address>thirdAddresses=new ArrayList<>();
        Address thirdAddress=new Address();
        thirdAddress.setCountryCode(380);
        thirdAddress.setLine1("Line1");
        thirdAddress.setLine2("Line2");
        customer.setAccounts(accounts);
        customer.setAddresses(addresses);
        secondCustomer.setAddresses(secondAddresses);
        secondCustomer.setAccounts(secondAccounts);
        thirdAccounts.add(secondAccount);
        thirdAccounts.add(fourthAccount);
        thirdAddresses.add(secondAddress);
        thirdCustomer.setAddresses(thirdAddresses);
        thirdCustomer.setAccounts(thirdAccounts);

        customerService.addCustomer(secondCustomer);
        customerService.addCustomer(customer);
        customerService.addCustomer(thirdCustomer);
        List<Customer>customers=customerService.getCustomersWithExpiredCards();

        Assert.assertNotNull(customers);
        Assert.assertEquals(2,customers.size());

    }

    @Test
    public void testGetCustomerByCardId(){
        Customer customer=new Customer();
        customer.setFirstName("Alex");
        customer.setLastName("Merser");
        ArrayList<Account>accounts=new ArrayList<>();
        Account account=new Account();
        account.setCardNumber("1616161616161616");
        account.setExpirationDate(LocalDate.now());
        account.setNameOnAccount("Alex");
        accounts.add(account);
        ArrayList<Address>addresses=new ArrayList<>();
        Address address=new Address();
        address.setCountryCode(380);
        address.setLine1("Line1");
        address.setLine2("Line2");
        addresses.add(address);
        customer.setAccounts(accounts);
        customer.setAddresses(addresses);
        Customer secondcustomer=new Customer();
        secondcustomer.setFirstName("Olekseii");
        secondcustomer.setLastName("Kaliuha");
        ArrayList<Account>secondaccounts=new ArrayList<>();
        Account secondaccount=new Account();
        secondaccount.setCardNumber("1717171717171717");
        secondaccount.setExpirationDate(LocalDate.now());
        secondaccount.setNameOnAccount("Alex");
        secondaccounts.add(secondaccount);
        ArrayList<Address>secondaddresses=new ArrayList<>();
        Address secondaddress=new Address();
        secondaddress.setCountryCode(380);
        secondaddress.setLine1("Line1");
        secondaddress.setLine2("Line2");
        secondaddresses.add(secondaddress);
        secondcustomer.setAccounts(secondaccounts);
        secondcustomer.setAddresses(secondaddresses);

        customerService.addCustomer(customer);
        customerService.addCustomer(secondcustomer);
        Customer customerFromBD=customerService.getCustomerByCardNumber("1717171717171717");

        Assert.assertNotNull(customerFromBD);
        Assert.assertEquals("Olekseii",customerFromBD.getFirstName());
        Assert.assertEquals("Kaliuha",customerFromBD.getLastName());
    }

    @Test
    public void testGetCustomerByAddress(){
        Customer customer=new Customer();
        customer.setFirstName("Alex");
        customer.setLastName("Merser");
        ArrayList<Account>accounts=new ArrayList<>();
        Account account=new Account();
        account.setCardNumber("1616161616161616");
        account.setExpirationDate(LocalDate.now());
        account.setNameOnAccount("Alex");
        accounts.add(account);
        ArrayList<Address>addresses=new ArrayList<>();
        Address address=new Address();
        address.setCountryCode(380);
        address.setLine1("Line1");
        address.setLine2("Line2");
        addresses.add(address);
        customer.setAccounts(accounts);
        customer.setAddresses(addresses);
        Customer secondcustomer=new Customer();
        secondcustomer.setFirstName("Olekseii");
        secondcustomer.setLastName("Kaliuha");
        ArrayList<Account>secondaccounts=new ArrayList<>();
        Account secondaccount=new Account();
        secondaccount.setCardNumber("1717171717171717");
        secondaccount.setExpirationDate(LocalDate.now());
        secondaccount.setNameOnAccount("Alex");
        secondaccounts.add(secondaccount);
        ArrayList<Address>secondaddresses=new ArrayList<>();
        Address secondaddress=new Address();
        secondaddress.setCountryCode(1);
        secondaddress.setLine1("Line3");
        secondaddress.setLine2("Line4");
        secondaddresses.add(secondaddress);
        secondcustomer.setAccounts(secondaccounts);
        secondcustomer.setAddresses(secondaddresses);
        Address addressForDB=new Address();
        addressForDB.setLine2("Line4");
        addressForDB.setLine1("Line3");
        addressForDB.setCountryCode(1);

        customerService.addCustomer(customer);
        customerService.addCustomer(secondcustomer);
        Customer customerFromBD=customerService.getCustomerByAddress(addressForDB);

        Assert.assertNotNull(customerFromBD);
        Assert.assertEquals("Olekseii",customerFromBD.getFirstName());
        Assert.assertEquals("Kaliuha",customerFromBD.getLastName());

    }

}
