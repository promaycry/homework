package com.epam.homework_17.repository;

import com.epam.homework_17.model.Customer;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import java.time.LocalDate;
import java.util.List;

public interface CustomerRepository extends MongoRepository<Customer,String> {

    @Query("{'firstName' : ?0 , 'lastName' : ?1}")
    Customer findCustomerByFirstNameAndLastName(String firstName,String lastName);

    @Query("{'accounts.cardNumber' : ?0}")
    Customer findCustomerByAccountCardNumber(String cardNumber);

    @Query("{'addresses.line1' : ?0 ,'addresses.line2' : ?1 , 'addresses.countryCode' : ?2 }")
    Customer findCustomerByAddress(String line1,String line2,int countryCode);

    @Query("{'accounts.expirationDate' : {$lte: ?0}}")
    List<Customer> findCustomersWithExpiredCards(LocalDate currentDate);

}
