package com.epam.homework_17.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.util.List;

@Document("Customer")
@Data
public class Customer {

    @Id
    private String id;

    private String firstName;

    private String lastName;

    private List<Account> accounts;

    private List<Address> addresses;

}
