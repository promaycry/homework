package com.epam.homework_17.model;

import lombok.Data;
import java.time.LocalDate;

@Data
public class Account {

    private String nameOnAccount;

    private String cardNumber;

    private LocalDate expirationDate;

}
