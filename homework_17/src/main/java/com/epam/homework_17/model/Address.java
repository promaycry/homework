package com.epam.homework_17.model;

import lombok.Data;

@Data
public class Address {

    private String line1;

    private String line2;

    private int countryCode;

}
