package com.epam.homework_17.service;

import com.epam.homework_17.model.Address;
import com.epam.homework_17.model.Customer;
import com.epam.homework_17.repository.CustomerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.util.List;

@Slf4j
@Service
public class CustomerService {

    private CustomerRepository customerRepository;

    @Autowired
    public CustomerService(CustomerRepository customerRepository){
        this.customerRepository=customerRepository;
    }

    public Customer addCustomer(Customer customer){
        return customerRepository.save(customer);
    }

    public Customer getCustomer(String id){
        return customerRepository.findById(id).get();
    }

    public Customer getCustomerByFirstNameAndLastName(String firstName, String lastName){
        return customerRepository.findCustomerByFirstNameAndLastName(firstName,lastName);
    }

    public Customer updateCustomer(Customer customer){
        if(customerRepository.existsById(customer.getId())){
          return customerRepository.save(customer);
        }
        else{
            throw new IllegalArgumentException("Customer doesn`t exist");
        }
    }

    public Customer getCustomerByCardNumber(String cardNumber){
        return customerRepository.findCustomerByAccountCardNumber(cardNumber);
    }

    public Customer getCustomerByAddress(Address address){
        return customerRepository.findCustomerByAddress(address.getLine1(),address.getLine2(),address.getCountryCode());
    }

    public List<Customer> getCustomersWithExpiredCards(){
        return customerRepository.findCustomersWithExpiredCards(LocalDate.now());
    }

}
