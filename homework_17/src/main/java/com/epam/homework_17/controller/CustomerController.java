package com.epam.homework_17.controller;

import com.epam.homework_17.model.Address;
import com.epam.homework_17.model.Customer;
import com.epam.homework_17.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
public class CustomerController {

    private CustomerService customerService;

    public CustomerController(CustomerService customerService){
        this.customerService=customerService;
    }

    @PutMapping("/customer")
    public Customer put(@RequestBody Customer customer){
        return customerService.addCustomer(customer);
    }

    @GetMapping("/customer/{id}")
    public Customer get(@PathVariable String id){
        return customerService.getCustomer(id);
    }

    @GetMapping("/customer/byNames")
    public Customer getCustomer(@RequestParam(name = "firstName")String firstName,
                        @RequestParam(name = "lastName")String lastName){
        return customerService.getCustomerByFirstNameAndLastName(firstName,lastName);
    }

    @GetMapping("/customer/byAddress")
    public Customer getCustomer(@RequestBody Address address){
        return customerService.getCustomerByAddress(address);
    }

    @PostMapping("/customer")
    public Customer updateCustomer(
                           @RequestBody Customer customer){
        return customerService.updateCustomer(customer);
    }

    @GetMapping("/customer/byAccountCardNumber")
    public Customer getCustomerByCardNumber(@RequestParam(name = "cardNumber")String cardNumber){
        return customerService.getCustomerByCardNumber(cardNumber);
    }

    @GetMapping("/customers/expiredCards")
    public List<Customer> getCustomersWithExpiredCards(){
        return customerService.getCustomersWithExpiredCards();
    }

}
