package com.epam.web.service;

import com.epam.dao.ExpositionDAO;
import com.epam.dao.LocaleDAO;
import com.epam.dao.UserDAO;
import com.epam.entity.Exposition;
import com.epam.entity.Hall;
import com.epam.entity.User;
import com.epam.util.ExpositionsQuery;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


@Service
public class ExpositionService extends MyService {

    private static final Integer ELEMENTS =6;

    @Autowired
    ExpositionDAO expositionDAO;

    @Autowired
    UserDAO userDAO;

    private static final long serialVersionUID = 1863978254689586513L;

    private static final Logger log = Logger.getLogger(ExpositionService.class);

    public String get(HttpSession session, int currentPage, String orderBy, Model model){

        String locale=(String)session.getAttribute("defaultLocale");

        String role="user";
        log.trace("user view set--->"+role);

        boolean admin=false;

        if(session.getAttribute("user")!=null){
            log.debug("select admin view");
            User user= (User) session.getAttribute("user");
            user=userDAO.getByID(user.getId());
            if(user.getRole().equals("ROLE_ADMIN")){
                role="admin";
                log.trace("user view set--->"+role);
                admin=true;
                log.debug("admin set--->"+admin);
            }
            user=userDAO.getByID(user.getId());
            session.setAttribute("user",user);
            log.trace("user update-->"+user);
        }


        log.trace("sorting way --->"+orderBy);

        log.trace("user view --->"+role);

        log.trace("locale--->"+locale);
        List<Exposition> expositionList=expositionDAO.getAll(locale,
                ExpositionsQuery.get(role,orderBy),currentPage, ELEMENTS);
        log.trace("expositions get --->"+expositionList);

        model.addAttribute("expositionList",expositionList);
        log.trace("expositionList set --->"+expositionList);
        model.addAttribute("sortBy",orderBy);
        log.trace("sortBy set --->"+orderBy);
        model.addAttribute("currentPage",currentPage);
        log.trace("currentPage set --->"+currentPage);

        int count=expositionDAO.getCount(admin);
        log.trace("count of expositions --->"+count);


        int numberOfPages=count/ ELEMENTS;
        log.trace("number of pages--->"+numberOfPages);

        if(count% ELEMENTS !=0){
            numberOfPages++;
            log.trace("number of pages--->"+numberOfPages);
        }

        model.addAttribute("noOfPages",numberOfPages);
        log.trace("number of pages sended--->"+numberOfPages);

        model.addAttribute("currentView","expositions");
        log.trace("set currentView-->expositions");

        return "jsp/view/expositionList";
    }

    public String deleteExposition(int id,int currentPage,String sort){
        log.debug("Command buyExposition starts");
        Exposition exposition=new Exposition();
        log.trace("exposition id-->"+id);
        exposition.setId(Long.valueOf(id));
        expositionDAO.delete(exposition);
        log.trace("exposition deleted -->"+exposition.getId());
        return"redirect:/expositionList?sortBy="+sort+"&currentPage="+currentPage;
    }

    public String add(HttpServletResponse response, HttpServletRequest request, MultipartFile multipartFile) throws IOException {
        log.debug("Command addExposition starts");
        Exposition exposition=new Exposition();

        HashMap<String,String> names=new HashMap<>();

        String tickets=request.getParameter("tickets");

        request.setAttribute("currentView","addPage");

        if(tickets.isEmpty()){
            String error="numberInput";
            request.setAttribute("errorMessage",error);
            log.error("error--->"+error);
            return "jsp/view/errorPage";
        }

        exposition.setTicketsNum(Integer.valueOf(tickets));

        String price =request.getParameter("price");

        if(price.isEmpty()||!price.matches("[1-9][0-9]{0,}[.]{0,1}[0-9]{0,2}")){
            String error="priceInput";
            request.setAttribute("errorMessage",error);
            log.error("error--->"+error);
            return "jsp/view/errorPage";
        }

        exposition.setCost(new BigDecimal(price));

        log.trace("price--->"+price);

        String date=request.getParameter("date");

        if(date.isEmpty() ||date.compareTo(String.valueOf(LocalDate.now()))<0){
            String error="dateLess";
            request.setAttribute("errorMessage",error);
            log.error("error--->"+error);
            return "jsp/view/errorPage";
        }

        exposition.setDate(Date.valueOf(date));

        log.trace("date--->"+date);

        List<String> locales= LocaleDAO.getLocales();
        for (String locale: locales) {
            String parameter=request.getParameter("name_"+locale);
            log.trace("parameter--->"+parameter);
            System.out.println(parameter);
            if(expositionDAO.getByTopic(new String(parameter.getBytes("ISO-8859-1"),"utf-8")
                    +"_"+locale)){
                String error="expositionExist";
                request.setAttribute("errorMessage",error);
                log.error("error--->"+error);
                return "jsp/view/errorPage";
            }
            else {
                log.trace("names put--->"+locale+","+new String(parameter.getBytes("ISO-8859-1"),
                        "utf-8")
                        +"_"+locale);
                names.put(locale,new String(parameter.getBytes("ISO-8859-1"),"utf-8")
                        +"_"+locale);
            }
        }

        log.trace("names count --->"+names.size());
        log.trace("locales--->"+locales.size());
        if(names.size()!=locales.size()){
            String error="allLocalesRequired";
            request.setAttribute("errorMessage",error);
            log.error("error--->"+error);
            return "jsp/view/errorPage";
        }

        log.trace("locale Topics --->"+names.toString());

        String[]halls=request.getParameterValues("halls");

        if(halls.length==0){
            String error="SelectHalls";
            request.setAttribute("errorMessage",error);
            log.error("error--->"+error);
            return "jsp/view/errorPage";
        }

        log.trace("halls-->"+ Arrays.toString(halls));

        List<Hall>hallTransofm=new ArrayList<>();
        for(String hall:halls){
            if(!hall.matches("[1-9]{1,}")){
                String error="hallNumber";
                request.setAttribute("errorMessage",error);
                log.error("error--->"+error);
                return "jsp/view/errorPage";
            }
            hallTransofm.add(new Hall(Long.valueOf(hall)));
            log.trace("hall --> "+hall);
        }


        InputStream inputStream=multipartFile.getInputStream();

        exposition.getHalls().addAll(hallTransofm);
        log.trace("exposition halls--->"+exposition.getHalls());

        expositionDAO.add(exposition,names,inputStream);
        log.trace("exposition add--->"+names.toString());

        log.debug("Command addExposition finished");
        return "redirect:/addExposition";
    }

}
