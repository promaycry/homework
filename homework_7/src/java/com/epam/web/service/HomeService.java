package com.epam.web.service;


import com.epam.dao.ExpositionDAO;
import com.epam.dao.LocaleDAO;
import com.epam.entity.Exposition;
import com.epam.entity.User;
import com.epam.util.CustomPasswordEncoder;
import com.epam.util.ExpositionsQuery;
import com.epam.util.Hash;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

@Service
public class HomeService {

    @Autowired
    ExpositionDAO expositionDAO;

    private static final Logger log = Logger.getLogger(HomeService.class);

    public String getIndex(Model model,
                           HttpServletRequest request,
                           HttpServletResponse response, HttpSession session,SettingService settingService){
        int currentPage=1;
        model.addAttribute("currentPage","1");
        model.addAttribute("sortBy","default");
        boolean admin=false;
        User user=(User)session.getAttribute("user");
        int count=expositionDAO.getCount(admin);
        int numberOfPages=count/ 6;
        if(count% 6 !=0){
            numberOfPages++;
        }
        model.addAttribute("noOfPages",numberOfPages);
        WebUtils.setSessionAttribute(request, SessionLocaleResolver.LOCALE_SESSION_ATTRIBUTE_NAME,"en");
        List<Exposition> expositionList=expositionDAO.getAll("ru",
                ExpositionsQuery.get("user","default"),currentPage, 6);
        model.addAttribute("expositionList",expositionList);
        settingService.setLocale(response,request,null);
        session.setAttribute("defaultLocale","en");
        session.setAttribute("locales", LocaleDAO.getLocales());
        return "jsp/view/expositionList";
    }
}
