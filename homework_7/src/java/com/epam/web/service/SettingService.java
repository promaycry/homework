package com.epam.web.service;

import com.epam.dao.UserDAO;
import com.epam.entity.User;
import com.epam.util.CustomPasswordEncoder;
import com.epam.util.Hash;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;

@Service
public class SettingService extends MyService {

    private static final long serialVersionUID = -2785976618667657267L;

    private static final Logger log = Logger.getLogger(SettingService.class);

    @Autowired
    UserDAO userDAO;

    @Autowired
    Hash hash;

    public String setting(HttpServletRequest request, HttpServletResponse response){
        log.debug("Command start");
        HttpSession httpSession=request.getSession();
        User user= (User) httpSession.getAttribute("user");
        String password="";
        String oldPassword="";
        String email="";
        String oldEmail="";
        if(user!=null){
            log.debug("authorized user setting");
            password= request.getParameter("newPassword");
            oldPassword=  request.getParameter("oldPassword");
            email= request.getParameter("newEmail");
            oldEmail= request.getParameter("oldEmail");
        }

        String locale=request.getParameter("locale");
        String defaultLocale= (String) httpSession.getAttribute("defaultLocale");

        if(locale==null){
            String errorMessage="locale.empty";
            request.setAttribute("errorMessage", errorMessage);
            log.error("errorMessage --> " + errorMessage);
            return "jsp/view/errorPage";
        }

        log.trace("default locale--->"+defaultLocale);
        log.trace("locale to set-->"+locale);

        if(!password.isEmpty()){
            if(!oldPassword.isEmpty()){
                log.trace("user current password--->"+user.getPassword());
                if(new CustomPasswordEncoder().matches(password,user.getPassword())){
                    log.debug("old password equals to user password");
                    userDAO.updateUserPassword(hash.hash(password),user);
                    user=userDAO.getByID(user.getId());
                    log.trace("user changed password --->"+user.getPassword());
                    user=userDAO.getByID(user.getId());
                    httpSession.setAttribute("user",user);
                    log.trace("user update-->"+user);
                }
                else {
                    String errorMessage="oldPassword.invalid";
                    request.setAttribute("errorMessage", errorMessage);
                    log.error("error--->"+errorMessage);
                    return "jsp/view/errorPage";
                }

            }
            else {
                String errorMessage="oldPassword.empty";
                request.setAttribute("errorMessage", errorMessage);
                log.error("error--->"+errorMessage);
                return "jsp/view/errorPage";
            }
        }

        if(!email.isEmpty()){
            log.trace("email to set--->"+email);
            if(!oldEmail.isEmpty()){
                log.trace("old email--->"+oldEmail);
                if(!new UserDAO().getByEmail(email)) {
                    log.debug("this email doesn`t exist in database");
                    if (oldEmail.equals(user.getEmail())) {
                        log.debug("old email equals to user email");
                        userDAO.updateUserEmail(email, user);
                        user.setEmail(email);
                        user=userDAO.getByID(user.getId());
                        httpSession.setAttribute("user",user);
                        log.trace("user update-->"+user);
                    }
                    else {
                        String errorMessage="oldEmail.invalid";
                        request.setAttribute("errorMessage", errorMessage);
                        log.error("error--->"+errorMessage);
                        return "jsp/view/errorPage";
                    }
                }
                else{
                    String errorMessage="newEmail.used";
                    request.setAttribute("errorMessage", errorMessage);

                    log.error("error--->"+errorMessage);
                    return "jsp/view/errorPage";
                }
            }
            else {
                String errorMessage="oldEmail.empty";
                request.setAttribute("errorMessage", errorMessage);

                log.error("error--->"+errorMessage);
                return "jsp/view/errorPage";
            }
        }

        if(!locale.equals(defaultLocale)){
            log.debug("setLocale");
            httpSession.setAttribute("defaultLocale",locale);
            log.trace("defaultLocale set --->"+locale);
            Config.set(httpSession,"javax.servlet.jsp.jstl.fmt.locale",locale);
            log.trace("jsp locale set --->"+locale);
        }

        String currentPage=request.getParameter("currentView");
        log.trace("currentPage-->"+currentPage);

        if(currentPage.equals("expositions")){
            String curPage=request.getParameter("currentPage");
            String sortBy=request.getParameter("sortBy");
            log.debug("redirect to list of expositions");
            return "redirect:/expositionList?sortBy="+sortBy+"&currentPage="+curPage;
        }
        else if(currentPage.equals("register")){
            log.debug("redirect to register page");
            return "redirect:/registerPage";
        }
        else if(currentPage.equals("users")){
            String curPage=request.getParameter("currentPage");
            String orderBy=request.getParameter("orderBy");
            log.debug("redirect to users page");
            return "redirect:/users?orderBy="+orderBy+"&currentPage="+curPage;
        }
        else if(currentPage.equals("orders")){
            String curPage=request.getParameter("currentPage");
            String orderBy=request.getParameter("orderBy");
            log.debug("redirect to orders page");
            return "redirect:/order?orderBy="+orderBy+"&currentPage="+curPage;
        }
        else if(currentPage.equals("addPage")){
            log.debug("redirect to page add");
            return "redirect:/addExposition";
        }
        else{
            return "redirect:/expositionList";
        }
    }


    public void setLocale(HttpServletResponse response,HttpServletRequest request,HttpSession session){
        LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);
        if(session==null){
            localeResolver.setLocale(request, response, StringUtils.parseLocaleString("en"));
            return;
        }
        localeResolver.setLocale(request, response,
                StringUtils.parseLocaleString(session.getAttribute("defaultLocale").toString()));
    }
}
