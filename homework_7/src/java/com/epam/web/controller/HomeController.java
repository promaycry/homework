package com.epam.web.controller;

import com.epam.annotation.Time;
import com.epam.dao.ExpositionDAO;
import com.epam.dao.HallDAO;
import com.epam.dao.LocaleDAO;
import com.epam.entity.Exposition;
import com.epam.entity.User;
import com.epam.util.ExpositionsQuery;
import com.epam.web.service.HomeService;
import com.epam.web.service.SettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class HomeController {

	@Autowired
	SettingService settingService;

	@Autowired
	HomeService homeService;

	@RequestMapping("/")
	@Time
	public String getIndex(Model model, HttpServletRequest request, HttpServletResponse response,HttpSession session){
		return homeService.getIndex(model,request,response,session,settingService);
	}

	@Time
	@RequestMapping(value = {"/registerPage"},method = {RequestMethod.GET, RequestMethod.POST})
	public String register(Model model,HttpServletRequest request, HttpServletResponse response,HttpSession session){
		settingService.setLocale(response,request,session);
		model.addAttribute("currentView","register");
		return "jsp/view/registerPage";
	}

	@Time
	@RequestMapping(value = {"/addExposition"},method = {RequestMethod.GET, RequestMethod.POST})
	public String addExposition(Model model){
		model.addAttribute("currentView","addPage");
		model.addAttribute("Halls",new HallDAO().getHalls());
		return "jsp/view/addExposition";
	}

}
