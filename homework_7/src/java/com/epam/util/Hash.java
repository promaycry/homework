package com.epam.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;



@Service
public final class Hash {

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    /**
     * Hash password
     * @param password
     *
     * @return hashedPassword
     *
     * @author Kaliuha Aleksei
     *
     */


    public String hash(String password) {
        return bCryptPasswordEncoder.encode(password);
    }

}
