package com.epam.entity;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Blob;
import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;


/**
 * Exposition entity
 *
 * @author Kaliuha Aleksei
 *
 */
public class Exposition extends Entity{

    private static final long serialVersionUID = 4716395168539434663L;

    private String topic;

    private LocalDate date;

    private BigDecimal cost;

    private Integer ticketsNum;

    private Integer visits;

    private List<Hall> halls=new ArrayList<>();

    private String image;


    public String getImage() {
        return image;
    }

    public void setImage(Blob blob) {
        try {
            InputStream inputStream =blob.getBinaryStream();
            ByteArrayOutputStream outputStream=new ByteArrayOutputStream();
            byte[]buffer=new byte[4096];
            int byteRead=1;
            while(true){
                try {
                    if (((byteRead=inputStream.read(buffer))==-1)) break;
                } catch (IOException e) {
                    e.printStackTrace();
                }
                outputStream.write(buffer,0,byteRead);
            }
            byte[]bytes=outputStream.toByteArray();
            this.image= Base64.getEncoder().encodeToString(bytes);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public Integer getTicketsNum() {
        return ticketsNum;
    }

    public void setTicketsNum(Integer ticketsNum) {
        this.ticketsNum = ticketsNum;
    }

    public Integer getVisits() {
        return visits;
    }

    public void setVisits(Integer visits) {
        this.visits = visits;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(Date newDate) {
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        date=LocalDate.parse(newDate.toString(),fmt);
    }

    public List<Hall> getHalls() {
        return halls;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    @Override
    public String toString() {
        return "Exposition [id "+getId()+" ,topic="+topic+ " ,halls= "+halls.toString()+",date= "+date+"]";
    }

}
