package com.epam.springcloud.notification;

import org.springframework.web.bind.annotation.*;

import java.util.*;


@RestController
@RequestMapping
public class NotificationController {

    private final Set<Notification> notifications = new HashSet<>();

    @PostMapping
    public Notification notify(@RequestParam String user) {
        Notification notification=new Notification();
        notification.setUser(user);
        notifications.add(notification);
        return notification;
    }

    @GetMapping
    public List<Notification> getNotifications() {
        return new ArrayList<>(notifications);
    }

}
