package com.epam.homework_12_1.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Data
@Entity
@EqualsAndHashCode(callSuper = false)
@DiscriminatorValue("CC")
public class CreditCard extends BillingDetails {

   private String cardNumber;

   private int expYear;

   private int expMonth;

}
