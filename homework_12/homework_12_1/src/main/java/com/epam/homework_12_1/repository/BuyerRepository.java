package com.epam.homework_12_1.repository;

import com.epam.homework_12_1.model.Buyer;

public interface BuyerRepository {

    void add(Buyer buyer);

    Buyer getByFirstNameAndLastName(String firstName, String lastName);

}
