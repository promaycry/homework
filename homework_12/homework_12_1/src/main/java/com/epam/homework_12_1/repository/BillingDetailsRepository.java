package com.epam.homework_12_1.repository;

import com.epam.homework_12_1.model.BillingDetails;

import java.util.List;

public interface BillingDetailsRepository {

    List<BillingDetails> getAllByUserId(Long id);

}
