package com.epam.homework_12_1.repository;

import com.epam.homework_12_1.model.BillingDetails;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class BillingDetailsRepositoryImpl implements BillingDetailsRepository {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional
    public List<BillingDetails> getAllByUserId(Long id) {
        Session session=sessionFactory.getCurrentSession();
        Query<BillingDetails> query=session.getNamedQuery("GetBillingDetailsByBuyerId");
        query.setParameter("buyer_id",id);
        List<BillingDetails>billingDetails=query.getResultList();
        return billingDetails;
    }

}
