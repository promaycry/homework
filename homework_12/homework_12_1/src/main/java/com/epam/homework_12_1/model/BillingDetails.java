package com.epam.homework_12_1.model;

import lombok.Data;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Data
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "BD_TYPE")
@NamedQueries({@NamedQuery(name = "GetBillingDetailsByBuyerId",
        query = "Select b from BillingDetails b where b.buyer.id=:buyer_id")})
@Table(name = "billing_details")
public abstract class BillingDetails {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private Buyer buyer;

}
