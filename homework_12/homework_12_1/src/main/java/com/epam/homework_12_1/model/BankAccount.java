package com.epam.homework_12_1.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Data
@Entity
@EqualsAndHashCode(callSuper = false)
@DiscriminatorValue("BA")
public class BankAccount extends BillingDetails {

    private String account;

    private String bandName;
}
