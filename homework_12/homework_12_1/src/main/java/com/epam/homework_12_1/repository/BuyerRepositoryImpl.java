package com.epam.homework_12_1.repository;

import com.epam.homework_12_1.model.Buyer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class BuyerRepositoryImpl implements BuyerRepository {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional
    public void add(Buyer buyer) {
        Session session=sessionFactory.getCurrentSession();
        session.save(buyer);
    }

    @Override
    @Transactional
    public Buyer getByFirstNameAndLastName(String firstName, String lastName) {
        Session session=sessionFactory.getCurrentSession();
        Query<Buyer> query = session.getNamedQuery("GetBuyerByLastNameAndFirstName");
        query.setParameter("lastName", lastName);
        query.setParameter("firstName",firstName);
        Buyer result = query.getSingleResult();
        return result;
    }

}
