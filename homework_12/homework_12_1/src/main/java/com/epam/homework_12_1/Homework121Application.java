package com.epam.homework_12_1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Homework121Application {

    public static void main(String[] args) {
        SpringApplication.run(Homework121Application.class, args);
    }

}
