package com.epam.homework_12_1.model;

import lombok.Data;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@NamedQueries({@NamedQuery(name = "GetBuyerByLastNameAndFirstName",
        query = "Select b from Buyer b where b.firstName=:firstName and b.lastName=:lastName")})
@Table(name = "buyers")
public class Buyer {

    @Id
    @GeneratedValue
    private Long id;

    private String firstName;

    private String lastName;

    @OneToMany(mappedBy = "buyer",cascade = CascadeType.ALL, orphanRemoval = true,fetch = FetchType.LAZY)
    private List<BillingDetails> billingDetails=new ArrayList<>();

}
