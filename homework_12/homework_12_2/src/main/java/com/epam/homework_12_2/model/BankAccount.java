package com.epam.homework_12_2.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@EqualsAndHashCode(callSuper = false)
@Table(name = "bank_accounts")
public class BankAccount extends BillingDetails {

    private String account;

    private String bandName;

}
