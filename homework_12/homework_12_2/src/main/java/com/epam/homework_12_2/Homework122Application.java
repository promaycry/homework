package com.epam.homework_12_2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Homework122Application {

    public static void main(String[] args) {
        SpringApplication.run(Homework122Application.class, args);
    }

}
