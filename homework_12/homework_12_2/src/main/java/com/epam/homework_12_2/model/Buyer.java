package com.epam.homework_12_2.model;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@NamedQueries({@NamedQuery(name = "GetBuyerByLastNameAndFirstName",
        query = "Select b from Buyer b where b.firstName=:firstName and b.lastName=:lastName")})
@Table(name = "buyers")
public class Buyer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String firstName;

    private String lastName;

    @OneToMany(mappedBy = "buyer",cascade = CascadeType.ALL, orphanRemoval = true,fetch = FetchType.LAZY)
    private List<BillingDetails> billingDetails=new ArrayList<>();

}
