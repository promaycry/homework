package com.epam.homework_12_2.model;


import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@NamedQueries({@NamedQuery(name = "GetBillingDetailsByBuyerId",
                query = "Select b from BillingDetails b where b.buyer.id=:buyer_id")
})
public abstract class BillingDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private Buyer buyer;

}
