package com.epam.homework_12_2.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@EqualsAndHashCode(callSuper = false)
@Table(name = "credit_cards")
public class CreditCard extends BillingDetails {

    private String cardNumber;

    private int expYear;

    private int expMonth;

}
