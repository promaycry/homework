package com.epam.homework_12_2.repository;


import com.epam.homework_12_2.model.Buyer;

public interface BuyerRepository {

    void add(Buyer buyer);

    Buyer getByFirstNameAndLastName(String firstName, String lastName);

}
