package com.epam.homework_12_2.repository;


import com.epam.homework_12_2.model.BillingDetails;

import java.util.List;

public interface BillingDetailsRepository {

    List<BillingDetails> getAllByUserId(Long id);

}
