package com.epam.homework_12_2.repository;

import com.epam.homework_12_2.config.TestConfig;
import com.epam.homework_12_2.model.BankAccount;
import com.epam.homework_12_2.model.BillingDetails;
import com.epam.homework_12_2.model.Buyer;
import com.epam.homework_12_2.model.CreditCard;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class})
public class BillingDetailsRepositoryTest {

    @Autowired
    BuyerRepository buyerRepository;

    @Autowired
    BillingDetailsRepository billingDetailsRepository;


    @Test
    @Transactional
    public void testAddAndGetAllByUserId(){

        Buyer buyer=new Buyer();
        buyer.setFirstName("Olekseii");
        buyer.setLastName("Kaliuha");
        CreditCard creditCard=new CreditCard();
        creditCard.setCardNumber("9999999999999999");
        creditCard.setExpMonth(12);
        creditCard.setExpYear(2020);
        creditCard.setBuyer(buyer);
        BankAccount bankAccount=new BankAccount();
        bankAccount.setBuyer(buyer);
        bankAccount.setBandName("PrivatBank");
        bankAccount.setAccount("promaycry");
        buyer.getBillingDetails().add(bankAccount);
        buyer.getBillingDetails().add(creditCard);


        Buyer buyer1=new Buyer();
        buyer1.setLastName("Savranskyy");
        buyer1.setFirstName("Vladimir");
        CreditCard creditCard1=new CreditCard();
        creditCard1.setBuyer(buyer1);
        creditCard1.setExpYear(2021);
        creditCard1.setExpMonth(10);
        creditCard1.setCardNumber("6666666666666666");
        buyer1.getBillingDetails().add(creditCard1);

        buyerRepository.add(buyer);
        buyerRepository.add(buyer1);

        Buyer newBuyer=buyerRepository.getByFirstNameAndLastName(buyer.getFirstName(),buyer.getLastName());

        Buyer newBuyer1=buyerRepository.getByFirstNameAndLastName(buyer1.getFirstName(),buyer1.getLastName());

        List<BillingDetails> billingDetailsListForBuyer= billingDetailsRepository.getAllByUserId(newBuyer.getId());

        List<BillingDetails> billingDetailsListForBuyer1= billingDetailsRepository.getAllByUserId(newBuyer1.getId());

        Assert.assertEquals(2,billingDetailsListForBuyer.size());
        Assert.assertEquals(1,billingDetailsListForBuyer1.size());


    }

}
