package com.epam.homework_5.service;

import com.epam.homework_5.model.User;
import com.epam.homework_5.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public List<User> getAll(Integer pageNo, Integer pageSize, String sortBy){
        Pageable paging =null;
        if(!sortBy.equals("null")){
            paging=PageRequest.of(pageNo,pageSize,Sort.by(sortBy));
        } else{
            paging=PageRequest.of(pageNo,pageSize);
        }
       Page<User> users=userRepository.findAll(paging);
        log.info("users from base-->"+users);
        if(users.hasContent()) {
            return users.getContent();
        } else {
            return new ArrayList<User>();
        }
    }

    public User get(Long id){
        User user=userRepository.getById(id);
        log.info("user from base-->"+user);
        return user;
    }

}
