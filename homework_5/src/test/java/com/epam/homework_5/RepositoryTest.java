package com.epam.homework_5;
import com.epam.homework_5.model.User;
import com.epam.homework_5.repository.UserRepository;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import net.bytebuddy.implementation.auxiliary.MethodCallProxy;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class RepositoryTest {

    @Autowired
    UserRepository userRepository;

    @Test
    public void testGetAll()  {
        List<User>users=userRepository.findAll();
        Assert.assertNotNull(users);
        Assert.assertEquals(3,users.size());
    }

    @Test
    public void testGet()  {
        User user=userRepository.getById(Long.valueOf(1));
        Assert.assertNotNull(user);
        Assert.assertEquals(Long.valueOf(1),user.getId());
    }

}
