package com.epam.homework_10_11_2.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Data
@DynamicInsert
@NoArgsConstructor
@NamedQueries({@NamedQuery(name = "platformByName",
        query = "from Platform e where e.name=:name")})
public class Platform {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(columnDefinition = "BINARY(16)",nullable = false,updatable = false)
    private UUID id;

    private String name;

    public Platform(String name){
        this.name=name;
    }

}
