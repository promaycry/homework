package com.epam.homework_10_11_2.entity;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Data
@DynamicInsert
public class Game {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(columnDefinition = "BINARY(16)",nullable = false,updatable = false)
    private UUID id;

    private String name;

    private String description;

    @ManyToOne
    private Engine engine;

    @ManyToMany(targetEntity = Platform.class,fetch = FetchType.LAZY)
    private List<Platform> platforms=new ArrayList<>();

    @ManyToOne(targetEntity = Company.class,fetch = FetchType.LAZY)
    @JoinColumn(name="company_id")
    private Company company;

}
