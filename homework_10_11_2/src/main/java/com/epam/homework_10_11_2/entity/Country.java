package com.epam.homework_10_11_2.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import java.util.UUID;

@Entity
@DynamicInsert
@DynamicUpdate
@Data
@NoArgsConstructor
@NamedQueries({@NamedQuery(name = "countryByName",
        query = "from Country c where c.name=:name")})
public class Country {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(columnDefinition = "BINARY(16)",nullable = false,updatable = false)
    private UUID id;

    private String name;

    public Country(String newName){
        this.name=newName;
    }

}
