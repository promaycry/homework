package com.epam.homework_10_11_2.dao;

import com.epam.homework_10_11_2.entity.Engine;

import java.util.UUID;

public interface EngineDao {
    void create(Engine engine);

    Engine get(UUID id);

    Engine get(String name);
}
