package com.epam.homework_10_11_2.dao;

import com.epam.homework_10_11_2.entity.Engine;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.UUID;

@Repository
public class EngineDaoImpl implements EngineDao{

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional
    public void create(Engine country) {
        Session session=sessionFactory.getCurrentSession();
        session.save(country);
    }

    @Override
    @Transactional
    public Engine get(UUID id) {
        Session session=sessionFactory.getCurrentSession();
        Engine country=session.get(Engine.class,id);
        return Objects.requireNonNull(country,"Country doesn`t exist");
    }

    @Override
    @Transactional
    public Engine get(String name) {
        Session session=sessionFactory.getCurrentSession();
        Object result=session.getNamedQuery("engineByName").setParameter("name",name).getSingleResult();
        return (Engine) result;
    }
}
