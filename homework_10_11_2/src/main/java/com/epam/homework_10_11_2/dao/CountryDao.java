package com.epam.homework_10_11_2.dao;

import com.epam.homework_10_11_2.entity.Country;

import java.util.UUID;

public interface CountryDao {

    void create(Country country);

    Country get(UUID id);

    Country get(String name);
}
