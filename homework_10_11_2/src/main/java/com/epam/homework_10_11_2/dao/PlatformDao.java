package com.epam.homework_10_11_2.dao;

import com.epam.homework_10_11_2.entity.Country;
import com.epam.homework_10_11_2.entity.Platform;

import java.util.List;

public interface PlatformDao {

    void create(Platform platform);

    Platform get(String name);

    List<Platform> getAll();

}
