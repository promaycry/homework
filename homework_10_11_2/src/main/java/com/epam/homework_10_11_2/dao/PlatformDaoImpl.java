package com.epam.homework_10_11_2.dao;

import com.epam.homework_10_11_2.entity.Country;
import com.epam.homework_10_11_2.entity.Engine;
import com.epam.homework_10_11_2.entity.Platform;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class PlatformDaoImpl implements PlatformDao{

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional
    public void create(Platform platform) {
        Session session=sessionFactory.getCurrentSession();
        session.save(platform);
    }

    @Override
    @Transactional
    public Platform get(String name) {
        Session session=sessionFactory.getCurrentSession();
        Object result=session.getNamedQuery("platformByName").setParameter("name",name).getSingleResult();
        return (Platform) result;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Platform> getAll() {
        List<Platform> result = sessionFactory.getCurrentSession().createQuery("from Platform").list();
        return result;
    }

}
