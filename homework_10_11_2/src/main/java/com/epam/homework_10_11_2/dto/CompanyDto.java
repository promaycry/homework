package com.epam.homework_10_11_2.dto;
import com.epam.homework_10_11_2.entity.Platform;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
public class CompanyDto {

    private UUID id;

    private String name;

    private List<GameDto>games=new ArrayList<>();

    private List<DeveloperDto>developers=new ArrayList<>();

    private UUID countryId;

}
