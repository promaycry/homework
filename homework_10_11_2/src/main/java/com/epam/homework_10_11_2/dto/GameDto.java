package com.epam.homework_10_11_2.dto;

import com.epam.homework_10_11_2.entity.Engine;
import com.epam.homework_10_11_2.entity.Platform;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
public class GameDto {

    private UUID id;

    private String name;

    private Engine engine;

    private List<Platform> platforms=new ArrayList<>();
}
