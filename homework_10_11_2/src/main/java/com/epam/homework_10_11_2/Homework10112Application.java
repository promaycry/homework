package com.epam.homework_10_11_2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;

@SpringBootApplication(exclude= HibernateJpaAutoConfiguration.class)
public class Homework10112Application {

    public static void main(String[] args) {
        SpringApplication.run(Homework10112Application.class, args);
    }

}
