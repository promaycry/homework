package com.epam.homework_10_11_2.service;

import com.epam.homework_10_11_2.entity.Platform;

import java.util.List;

public interface PlatformService {
    Platform create(Platform platform);
    Platform get(String name);
    List<Platform> getAll();
}
