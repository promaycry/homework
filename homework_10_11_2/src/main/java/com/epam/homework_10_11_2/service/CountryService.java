package com.epam.homework_10_11_2.service;

import com.epam.homework_10_11_2.entity.Country;

import java.util.UUID;

public interface CountryService {

    Country create(Country country);

    Country get(UUID id);

    Country get(String name);
}
