package com.epam.homework_10_11_2.service;

import com.epam.homework_10_11_2.dao.CompanyDao;
import com.epam.homework_10_11_2.dto.CompanyDto;
import com.epam.homework_10_11_2.entity.Company;
import com.epam.homework_10_11_2.entity.Developer;
import com.epam.homework_10_11_2.entity.Game;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@Transactional
public class CompanyServiceImpl implements CompanyService {

    @Autowired
    private CompanyAssembler companyAssembler;

    @Autowired
    private CompanyDao companyDao;

    @Override
    public CompanyDto create(CompanyDto dto) {
        Company company = companyAssembler.assemble(dto);
        companyDao.save(company);
        CompanyDto result = companyAssembler.assemble(company);
        return result;
    }

    @Override
    public CompanyDto get(String name) {
        Company company = companyDao.get(name);
        return companyAssembler.assemble(company);
    }

    @Override
    public CompanyDto get(UUID uuid) {
        Company company = companyDao.get(uuid);
        return companyAssembler.assemble(company);
    }

    @Override
    public void delete(UUID id) {
        companyDao.delete(id);
    }

    @Override
    @Transactional
    public CompanyDto update(CompanyDto companyDto) {
        Company updatedCompany = companyAssembler.assemble(companyDto);
        Company company = companyDao.get(companyDto.getId());
        company.setName(updatedCompany.getName());
        updateApplications(company.getGames(), updatedCompany.getGames());
        updateDevelopers(company.getDevelopers(),updatedCompany.getDevelopers());
        return get(updatedCompany.getName());
    }

    private void updateApplications(List<Game> persistentApplications, List<Game> newApplications) {
        Map<UUID, Game> stillExistentJourneys = newApplications
                .stream()
                .filter(j -> Objects.nonNull(j.getId()))
                .collect(Collectors.toMap(Game::getId, Function.identity()));

        List<Game> applicationsToAdd = newApplications
                .stream()
                .filter(j -> Objects.isNull(j.getId()))
                .collect(Collectors.toList());

        Iterator<Game> persistentIterator = persistentApplications.iterator();
        while (persistentIterator.hasNext()) {
            Game persistentJourney = persistentIterator.next();
            if (stillExistentJourneys.containsKey(persistentJourney.getId())) {
                Game updatedJourney = stillExistentJourneys.get(persistentJourney.getId());
                updateGame(persistentJourney, updatedJourney);
            } else {
                persistentIterator.remove();
                persistentJourney.setCompany(null);
            }
        }
        persistentApplications.addAll(applicationsToAdd);
    }

    private void updateDevelopers(List<Developer> persistentDevelopers, List<Developer> newDevelopers) {
        Map<UUID, Developer> stillExistentDevelopers = newDevelopers
                .stream()
                .filter(j -> Objects.nonNull(j.getId()))
                .collect(Collectors.toMap(Developer::getId, Function.identity()));

        List<Developer> developersToAdd = newDevelopers
                .stream()
                .filter(j -> Objects.isNull(j.getId()))
                .collect(Collectors.toList());

        Iterator<Developer> persistentIterator = persistentDevelopers.iterator();
        while (persistentIterator.hasNext()) {
            Developer persistentDeveloper = persistentIterator.next();
            if (stillExistentDevelopers.containsKey(persistentDeveloper.getId())) {
                Developer updatedDeveloper = stillExistentDevelopers.get(persistentDeveloper.getId());
                updateDeveloper(persistentDeveloper, updatedDeveloper);
            } else {
                persistentIterator.remove();
                persistentDeveloper.setCompany(null);
            }
        }
        persistentDevelopers.addAll(developersToAdd);
    }

    private void updateGame(Game persistentGame, Game updatedGame) {
        persistentGame.setCompany(updatedGame.getCompany());
        persistentGame.setName(updatedGame.getName());
        persistentGame.setEngine(updatedGame.getEngine());
        persistentGame.setDescription(updatedGame.getDescription());
        persistentGame.setPlatforms(updatedGame.getPlatforms());
    }

    private void updateDeveloper(Developer persistentDeveloper, Developer updatedDeveloper) {
        persistentDeveloper.setCompany(updatedDeveloper.getCompany());
        persistentDeveloper.setName(updatedDeveloper.getName());
    }

}
