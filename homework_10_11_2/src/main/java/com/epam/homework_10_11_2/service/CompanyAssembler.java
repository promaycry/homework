package com.epam.homework_10_11_2.service;

import com.epam.homework_10_11_2.dto.CompanyDto;
import com.epam.homework_10_11_2.entity.Company;

public interface CompanyAssembler {

    Company assemble(CompanyDto companyDto);

    CompanyDto assemble(Company company);

}
