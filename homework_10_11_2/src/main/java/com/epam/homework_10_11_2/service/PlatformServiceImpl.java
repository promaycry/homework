package com.epam.homework_10_11_2.service;

import com.epam.homework_10_11_2.dao.PlatformDao;
import com.epam.homework_10_11_2.entity.Engine;
import com.epam.homework_10_11_2.entity.Platform;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlatformServiceImpl implements PlatformService{

    @Autowired
    PlatformDao platformDao;

    @Override
    public Platform create(Platform platform) {
        platformDao.create(platform);
        return platformDao.get(platform.getName());
    }

    @Override
    public Platform get(String name) {
        return platformDao.get(name);
    }

    @Override
    public List<Platform> getAll() {
        return platformDao.getAll();
    }
    
}
