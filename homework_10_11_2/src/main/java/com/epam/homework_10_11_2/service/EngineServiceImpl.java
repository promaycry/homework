package com.epam.homework_10_11_2.service;


import com.epam.homework_10_11_2.dao.EngineDao;
import com.epam.homework_10_11_2.entity.Engine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class EngineServiceImpl implements EngineService {

    @Autowired
    EngineDao engineDao;

    @Override
    public Engine create(Engine engine) {
        engineDao.create(engine);
        return engineDao.get(engine.getName());
    }

    @Override
    public Engine get(UUID id) {
        return engineDao.get(id);
    }

    @Override
    public Engine get(String name) {
        return engineDao.get(name);
    }

}
