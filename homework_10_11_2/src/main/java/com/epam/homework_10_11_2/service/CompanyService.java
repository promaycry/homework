package com.epam.homework_10_11_2.service;

import com.epam.homework_10_11_2.dto.CompanyDto;

import java.util.UUID;

public interface CompanyService {

    CompanyDto create(CompanyDto companyDto);

    CompanyDto get(String name);

    void delete(UUID id);

    CompanyDto update(CompanyDto companyDto);

    public CompanyDto get(UUID uuid);

}
