package com.epam.homework_10_11_2.service;

import com.epam.homework_10_11_2.dao.CountryDao;
import com.epam.homework_10_11_2.dao.EngineDao;
import com.epam.homework_10_11_2.dto.CompanyDto;
import com.epam.homework_10_11_2.dto.DeveloperDto;
import com.epam.homework_10_11_2.dto.GameDto;
import com.epam.homework_10_11_2.entity.Company;
import com.epam.homework_10_11_2.entity.Developer;
import com.epam.homework_10_11_2.entity.Game;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CompanyAssemblerImpl implements CompanyAssembler{

    @Autowired
    CountryDao countryDao;

    @Override
    public Company assemble(CompanyDto companyDto) {
        Company company=new Company();
        company.setName(companyDto.getName());
        company.setId(companyDto.getId());
        companyDto.getGames().forEach(gameDto -> company.getGames()
                .add(assemble(gameDto,company)));
        companyDto.getDevelopers().forEach(developerDto -> company.getDevelopers()
                .add(assemble(developerDto,company)));
        company.setCountry(countryDao.get(companyDto.getCountryId()));
        return company;
    }

    @Override
    public CompanyDto assemble(Company company) {
        CompanyDto companyDto=new CompanyDto();
        companyDto.setName(company.getName());
        companyDto.setId(company.getId());
        company.getGames()
                .forEach(game -> companyDto.getGames()
                        .add(assemble(game)));
        company.getDevelopers().forEach(developer -> companyDto.getDevelopers()
                .add(assemble(developer)));
        companyDto.setCountryId(company.getCountry().getId());
        return companyDto;
    }

    public GameDto assemble(Game game){
        GameDto gameDto =new GameDto();
        gameDto.setId(game.getId());
        gameDto.setName(game.getName());
        gameDto.setEngine(game.getEngine());
        gameDto.setPlatforms(game.getPlatforms());
        return gameDto;
    }

    public Game assemble(GameDto gameDto, Company company){
        Game game =new Game();
        game.setId(gameDto.getId());
        game.setName(gameDto.getName());
        game.setCompany(company);
        game.setEngine(gameDto.getEngine());
        game.setPlatforms(gameDto.getPlatforms());
        return game;
    }

    public DeveloperDto assemble(Developer developer){
        DeveloperDto developerDto=new DeveloperDto();
        developerDto.setBirthDate(developer.getBirthDate());
        developerDto.setName(developer.getName());
        developerDto.setId(developer.getId());
        return developerDto;
    }

    public Developer assemble(DeveloperDto developerDto, Company company){
        Developer developer =new Developer();
        developer.setId(developerDto.getId());
        developer.setName(developerDto.getName());
        developer.setBirthDate(developerDto.getBirthDate());
        developer.setCompany(company);
        return developer;
    }

}
