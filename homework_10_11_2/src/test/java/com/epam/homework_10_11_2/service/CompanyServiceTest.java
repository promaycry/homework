package com.epam.homework_10_11_2.service;

import com.epam.homework_10_11_2.config.TestConfig;
import com.epam.homework_10_11_2.dto.CompanyDto;
import com.epam.homework_10_11_2.dto.DeveloperDto;
import com.epam.homework_10_11_2.dto.GameDto;
import com.epam.homework_10_11_2.entity.Country;
import com.epam.homework_10_11_2.entity.Engine;
import com.epam.homework_10_11_2.entity.Platform;
import lombok.AllArgsConstructor;
import net.bytebuddy.utility.RandomString;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class})
public class CompanyServiceTest {

    @Autowired
    private CompanyService companyService;

    @Autowired
    private CountryService countryService;

    @Autowired
    private EngineService engineService;

    @Autowired
    private PlatformService platformService;

    private static final String[] COUNTRY_NAMES = {"Ukraine", "America","Japane" };

    private static final String[] ENGINE_NAMES = {"Unity", "UnrealEngine","CryEngine","4A Engine","Source"};

    private static final String[] PLATFORMS={"PC","PS","XBOX"};

    @Before
    public void createCountries() {
        Stream.of(COUNTRY_NAMES)
                .forEach(name -> countryService.create(new Country(name)));

        Stream.of(ENGINE_NAMES)
                .forEach(name -> engineService.create(new Engine(name)));

        Stream.of(PLATFORMS)
                .forEach(name -> platformService.create(new Platform(name)));
    }

    @Test
    @Transactional
    public void testCreateCompany() {
        CompanyDto companyDto=companyDto("EPIC GAMES","America",30,"Fortnite");
        companyDto.getGames().forEach(gameDto ->gameDto.setEngine(engineService.get("UnrealEngine")));
        companyDto.getGames().forEach(gameDto -> gameDto.setPlatforms(platformService.getAll()));
        CompanyDto createdCompany = companyService.create(companyDto);
        Assert.assertEquals(companyDto.getGames().size(), createdCompany.getGames().size());
        Assert.assertEquals(companyDto.getCountryId(),createdCompany.getCountryId());
        Assert.assertEquals(companyDto.getName(),createdCompany.getName());
        Assert.assertEquals(companyDto.getDevelopers().size(),createdCompany.getDevelopers().size());
        Assert.assertEquals(3,createdCompany.getGames().get(0).getPlatforms().size());
        Assert.assertNotNull(createdCompany.getId());
    }

    private List<DeveloperDto>developers(Integer number){
        List<DeveloperDto>developers=new ArrayList<>();
        for(int i=0;i<number;i++){
            DeveloperDto developer=new DeveloperDto();
            developer.setBirthDate(Date.valueOf(LocalDate.now()));
            developer.setName(RandomString.make(10));
            developers.add(developer);
        }
        return developers;
    }

    @Test
    @Transactional
    public void testGetCompany() {
        String name="4A GAMES";
        CompanyDto companyDto=companyDto(name,"Ukraine",10,"Metro Exodus",
                "Metro 2033","Metro 2033 Last Light");
        companyDto.getGames().forEach(gameDto ->gameDto.setEngine(engineService.get("4A Engine")));
        companyDto.getGames().forEach(gameDto -> gameDto.setPlatforms(platformService.getAll()));
        CompanyDto createdCompany = companyService.create(companyDto);
        CompanyDto gettedCompany = companyService.get(name);
        Assert.assertEquals(createdCompany.getGames().size(), gettedCompany.getGames().size());
        Assert.assertEquals(createdCompany.getGames(), gettedCompany.getGames());
        Assert.assertEquals(createdCompany.getCountryId(),gettedCompany.getCountryId());
        Assert.assertEquals(createdCompany.getName(),gettedCompany.getName());
        Assert.assertEquals(createdCompany.getId(),gettedCompany.getId());
        Assert.assertEquals(3,gettedCompany.getGames().get(0).getPlatforms().size());
        Assert.assertEquals(createdCompany.getDevelopers().size(),gettedCompany.getDevelopers().size());
    }

    @Test
    @Transactional
    public void testDeleteCompany() {
        CompanyDto companyDto = companyDto("FacePunch","America",100,"Rust","Garry`s Mod");
        companyDto.getGames().get(0).setEngine(engineService.get("Unity"));
        companyDto.getGames().get(1).setEngine(engineService.get("Source"));
        companyDto.getGames().get(0).setPlatforms(Arrays.asList(platformService.get("PC")));
        companyDto.getGames().get(1).setPlatforms(Arrays.asList(platformService.get("PC")));
        CompanyDto createdTourist = companyService.create(companyDto);
        companyService.delete(createdTourist.getId());
        try {
            companyService.get(createdTourist.getId());
        }
        catch (Exception e){
            Assert.assertEquals("Company doesn`t exist",e.getMessage());
            return;
        }
        Assert.fail("Company still exist");
    }

    @Test
    public void testUpdateCompany() {
        CompanyDto companyDto = companyDto("CryTek","Japane",
                40,"Crysis");
        companyDto.getGames().get(0).setEngine(engineService.get("CryEngine"));
        companyDto.getGames().get(0).getPlatforms().add(platformService.get("PC"));
        CompanyDto createdCompany = companyService.create(companyDto);
        Assert.assertEquals(1,createdCompany.getGames().get(0).getPlatforms().size());
        GameDto product=createdCompany.getGames().get(0);
        product.setName("Crysis 1");
        GameDto gameDto =new GameDto();
        gameDto.setName("Crysis 2");
        gameDto.setEngine(engineService.get("CryEngine"));
        gameDto.getPlatforms().add(platformService.get("PC"));
        createdCompany.getGames().add(gameDto);
        String newName="CryTek INC";
        createdCompany.setName(newName);
        DeveloperDto gettedDeveloper=createdCompany.getDevelopers().get(0);
        gettedDeveloper.setName("Dmytro Kolesnikov");
        DeveloperDto developerDto=new DeveloperDto();
        developerDto.setName("Aleksei Kaliuha");
        developerDto.setBirthDate(Date.valueOf(LocalDate.now()));
        createdCompany.getDevelopers().add(developerDto);

        Assert.assertEquals(1,createdCompany.getGames().get(0).getPlatforms().size());

        CompanyDto updatedCompany=companyService.update(createdCompany);

        Assert.assertEquals(1,updatedCompany.getGames().get(0).getPlatforms().size());

        CompanyDto newCompany=companyService.get(newName);
        Assert.assertEquals(1,newCompany.getGames().get(0).getPlatforms().size());
        Assert.assertNotNull(newCompany);
        Assert.assertEquals(updatedCompany.getId(),newCompany.getId());


        Assert.assertNotEquals(companyDto.getDevelopers().size(),newCompany.getDevelopers().size());
        Assert.assertNotEquals(companyDto.getName(),updatedCompany.getName());
        Assert.assertEquals(createdCompany.getId(),updatedCompany.getId());
        Assert.assertNotEquals(companyDto.getGames().size(),updatedCompany.getGames().size());
        Assert.assertEquals(2,newCompany.getGames().size());
        Assert.assertEquals(newName,newCompany.getName());
        Assert.assertEquals(companyDto.getCountryId(),updatedCompany.getCountryId());
    }

    private CompanyDto companyDto(String name,String country,Integer number,String...games){
        CompanyDto companyDto = new CompanyDto();
        companyDto.setName(name);
        companyDto.setCountryId(countryService.get(country).getId());
        companyDto.setDevelopers(developers(number));
        for (String productName: games ) {
            companyDto.getGames().add(game(productName));
        }
        return companyDto;
    }

    private GameDto game(String name){
        GameDto gameDto =new GameDto();
        gameDto.setName(name);
        return gameDto;
    }

}
