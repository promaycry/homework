package com.epam.homework_5;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories("com.epam.homework_5.repository")
@EntityScan("com.epam.homework_5.model")
public class Homework14Application {

    public static void main(String[] args) {
        SpringApplication.run(Homework14Application.class, args);
    }

}
