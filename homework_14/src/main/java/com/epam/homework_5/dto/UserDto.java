package com.epam.homework_5.dto;

import lombok.Data;

@Data
public class UserDto {

    private String login;

    private String email;

    private String password;

}
