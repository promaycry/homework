package com.epam.homework_5.healtchecker;

import org.springframework.boot.actuate.health.CompositeHealthContributor;
import org.springframework.boot.actuate.health.HealthContributor;
import org.springframework.boot.actuate.health.NamedContributor;
import org.springframework.stereotype.Component;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

@Component("Homework14Application")
public class UserApplicationHealthContributor implements CompositeHealthContributor {

    private Map<String, HealthContributor>
            contributors = new LinkedHashMap<>();

    public UserApplicationHealthContributor() {
        contributors.put("UserApplication",
                new UserApplicationHealthIndicator());
    }

    @Override
    public Iterator<NamedContributor<HealthContributor>> iterator() {
        return contributors.entrySet().stream()
                .map((entry) ->
                        NamedContributor.of(entry.getKey(),
                                entry.getValue())).iterator();
    }

    @Override
    public HealthContributor getContributor(String name) {
        return contributors.get(name);
    }

}
