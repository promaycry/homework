package com.epam.homework_5.healtchecker;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

import java.net.Socket;

@Component("UserApplication")
public class UserApplicationHealthIndicator implements HealthIndicator {

    private static final String URL="http://localhost:8080/users";

    @Override
    public Health health() {
        try (Socket socket = new Socket(new java.net.URL(URL).getHost(),8080)) {
        } catch (Exception e) {
            return Health.down()
                    .withDetail("error", e.getMessage())
                    .build();
        }
        return Health.up().build();
    }

}
