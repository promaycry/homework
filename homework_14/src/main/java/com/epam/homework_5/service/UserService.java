package com.epam.homework_5.service;

import com.epam.homework_5.dto.UserDto;
import com.epam.homework_5.model.User;
import com.epam.homework_5.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Transactional
    public List<User> getAll(Integer pageNo, Integer pageSize, String sortBy){
        Pageable paging =null;
        if(!sortBy.equals("null")){
            paging=PageRequest.of(pageNo,pageSize,Sort.by(sortBy));
        } else{
            paging=PageRequest.of(pageNo,pageSize);
        }
       Page<User> users=userRepository.findAll(paging);
        if(users.hasContent()) {
            return users.getContent();
        } else {
            return new ArrayList<>();
        }
    }

    @Transactional
    public User get(Long id){
        User user=userRepository.getById(id);
        return user;
    }

    @Transactional
    public User get(String userName){
        User user=userRepository.getByLogin(userName);
        return user;
    }

    @Transactional
    public User create(UserDto userDto){
        User user=new User();
        user.setLogin(userDto.getLogin());
        user.setEmail(userDto.getEmail());
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        return userRepository.save(user);
    }

    @Transactional
    public void delete(String userName){
        userRepository.deleteByLogin(userName);
    }

    @Transactional
    public User update(User user){
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

}
