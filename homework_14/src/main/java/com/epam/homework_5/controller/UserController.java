package com.epam.homework_5.controller;


import com.epam.homework_5.dto.UserDto;
import com.epam.homework_5.model.User;
import com.epam.homework_5.service.UserService;
import io.micrometer.core.annotation.Timed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Timed
@RequestMapping("/users")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping
    @ResponseBody
    public List<User> get(@RequestParam(defaultValue = "0") Integer pageNo,
                          @RequestParam(defaultValue = "3") Integer pageSize,
                          @RequestParam(defaultValue = "null") String sortBy){
        return userService.getAll(pageNo,pageSize,sortBy);
    }

    @PostMapping(value = "/{id}")
    @ResponseBody
    public User get(@PathVariable Long id){
        return userService.get(id);
    }

    @GetMapping(value = "/{login}")
    @ResponseBody
    public User get(@PathVariable String login){
        return userService.get(login);
    }

    @PutMapping
    @ResponseBody
    public User create(@RequestBody UserDto userDto){
        return userService.create(userDto);
    }

    @DeleteMapping(value = "/{login}")
    public void delete(@PathVariable String login){
        userService.delete(login);
    }

    @PostMapping
    public User update(@RequestBody User user){
        return userService.update(user);
    }

}
