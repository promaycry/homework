package com.epam.homework_5.model;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * User entity.
 *
 * @author Kaliuha Aleksei
 *
 */
@Entity
@Table(name = "users")
@Data
public class User {
    
    @Id
    @GeneratedValue
    private Long id;

    private static final long serialVersionUID = -6889036256149495388L;

    @Column(name = "login",nullable = false,unique = true)
    private String login;

    @Column(name = "email",nullable = false,unique = true)
    private String email;

    @Column(name = "balance",nullable = false)
    private BigDecimal balance=new BigDecimal("0");

    @Column(name = "password",nullable = false,unique = true)
    private String password;

}
