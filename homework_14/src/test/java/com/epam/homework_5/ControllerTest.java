package com.epam.homework_5;

import com.epam.homework_5.controller.UserController;
import com.epam.homework_5.model.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Homework14Application.class)
public class ControllerTest {


    @Autowired
    UserController userController;


    @Test
    public void getAllTest(){
        List<User>users=userController.get(0,3,"email");
        Assert.assertNotNull(users);
        Assert.assertEquals(3,users.size());
    }


    @Test
    public void getTest(){
        User user=userController.get((long)1);
        Assert.assertNotNull(user);
        Assert.assertEquals("admin",user.getLogin());
    }

}
