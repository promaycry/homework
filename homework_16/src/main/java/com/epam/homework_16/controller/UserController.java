package com.epam.homework_16.controller;

import com.epam.homework_16.entity.User;
import com.epam.homework_16.entity.UserDTO;
import com.epam.homework_16.service.UserService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {

    private UserService userService;

    public UserController(UserService userService){
        this.userService=userService;
    }

    @GetMapping("/{id}")
    public User get(@PathVariable Long id){
        return userService.get(id);
    }

    @PutMapping
    public User add(@RequestBody UserDTO userDTO){
        return userService.add(userDTO);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id){
        userService.delete(id);
    }

    @PostMapping("/{id}")
    public User update(@RequestParam(name = "newEmail") String newEmail,@PathVariable Long id){
       return userService.update(id,newEmail);
    }

}
