package com.epam.homework_16.entity;

import lombok.Data;

@Data
public class UserDTO {
    private String login;
    private String email;
}
