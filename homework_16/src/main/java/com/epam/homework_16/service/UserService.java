package com.epam.homework_16.service;

import com.epam.homework_16.entity.User;
import com.epam.homework_16.entity.UserDTO;
import com.epam.homework_16.repository.UserRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class UserService {

    private UserRepository userRepository;

    public UserService(UserRepository userRepository){
        this.userRepository=userRepository;
    }

    @Transactional
    public User get(Long id){
      return userRepository.findById(id).get();
    }

    @Transactional
    public User add(UserDTO userDTO){
        User user=new User();
        user.setEmail(userDTO.getEmail());
        user.setLogin(userDTO.getLogin());
        return userRepository.save(user);
    }

    @Transactional
    public void delete(Long id){
        userRepository.delete(userRepository.getOne(id));
    }

    @Transactional
    public User update(Long id,String email){
        User user=userRepository.getOne(id);
        user.setEmail(email);
        return userRepository.save(user);
    }

}
